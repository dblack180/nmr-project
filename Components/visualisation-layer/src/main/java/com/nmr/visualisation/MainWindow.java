package com.nmr.visualisation;

import java.util.Random;

import org.lwjgl.LWJGLException;
import org.lwjgl.Sys;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;
import org.lwjgl.opengl.GL11;
import org.lwjgl.util.glu.GLU;

public class MainWindow {
    double lastFPS = 0;
    double fps = 0;
    int seed = 0;
    float rotation = 0;
    float[][]noise = new float[0][0];
    float[]cameraPos = {0f,0f,0f};

    public static void main(String[] args)
    {
        MainWindow mainClass = new MainWindow();
        Random r = new Random();
        mainClass.seed = r.nextInt();
        mainClass.launchScreen();
    }

    float lerp(float x0, float x1, float alpha)
    {
       return x0 * (1 - alpha) + alpha * x1;
    }

    /**
     * Calculate the FPS and set it in the title bar
     */
    public void updateFPS() {
        if (getTime() - lastFPS > 1000) {
            Display.setTitle("FPS: " + fps + "; Seed: " + seed);
            
            fps = 0; //reset the FPS counter
            lastFPS += 1000; //add one second
        }
        fps++;
    }

    public void launchScreen()
    {
        try {
            Display.setDisplayMode(new DisplayMode(800,600));
            Display.create();
        } catch (LWJGLException e) {
            e.printStackTrace();
            System.exit(0);
        }

        lastFPS = getTime();
        float rot = 0.0f; 
//        PerlinGen pg = new PerlinGen();
//        noise = pg.GeneratePerlinNoise(pg.GenerateWhiteNoise(100, 100, seed), 5); //Get perlin noise array, returns float values from 0.0 to 1.0.
        System.out.println("Noise length is: (" + noise.length + "," + noise[0].length + ")");

        // init OpenGL here
        GL11.glMatrixMode(GL11.GL_PROJECTION);
        GL11.glLoadIdentity();
        GL11.glOrtho(0, 800, 0, 600, 1, -1);
        GL11.glMatrixMode(GL11.GL_MODELVIEW);

        GL11.glPolygonMode( GL11.GL_FRONT_AND_BACK, GL11.GL_LINE ); //Wireframe
        //GL11.glPolygonMode( GL11.GL_FRONT_AND_BACK, GL11.GL_FILL ); //Normal

        while (!Display.isCloseRequested()) {

            updateFPS();

            // render OpenGL here
            GL11.glClearColor(1,1,1,1);
            GL11.glClear(GL11.GL_COLOR_BUFFER_BIT | GL11.GL_DEPTH_BUFFER_BIT);

            // draw quad
            int centerX = noise.length*5;
            int centerY = 5;
            int centerZ = noise[0].length*-5;
            GLU.gluLookAt(centerX*2,500, centerZ*2, /* look from camera XYZ */
                    centerX, centerY, centerZ, /* look at the origin */
                     0, 1, 0); /* positive Y up vector */

            drawNoiseGrid(rot);
            rot += 0.05f;

            Display.update();
        }

        Display.destroy();
    }

    public void drawNoiseGrid(float r)
    {
        rotation = r;
        GL11.glColor3f(0f,0f,0.0f);

        for(int x=0;x<noise.length-1;x++)
        {
            for(int y=0;y<noise[x].length-1;y++)
            {
                GL11.glBegin(GL11.GL_QUADS);
                GL11.glVertex3f(10*x,noise[x][y]*10,-10*y);//topleft
                GL11.glVertex3f(10*(x+1),noise[x+1][y]*10,-10*y);//topright
                GL11.glVertex3f(10*x,noise[x][y+1]*10,-10*(y+1));//bottomleft
                GL11.glVertex3f(10*(x+1),noise[x+1][y+1]*10,-10*(y+1));//bottomright
                GL11.glEnd();
            }
        }
    }

    /**
     * Get the time in milliseconds
     * 
     * @return The system time in milliseconds
     */
    public long getTime() {
        return (Sys.getTime() * 1000) / Sys.getTimerResolution();
    }
}