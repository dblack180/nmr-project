package com.nmr.visualisation;

/**
 * Just a simple proof of concept to try and generate a 3D interactive
 * Line Plot of the NMR Spectra for Rattlesnake Venom
 * 
 * @date 4/02/2015
 * @author Davey
 *
 */

import java.awt.Dimension;
import java.awt.Toolkit;
import java.util.ArrayList;
import java.util.List;

import org.jzy3d.analysis.AbstractAnalysis;
import org.jzy3d.analysis.AnalysisLauncher;
import org.jzy3d.chart.factories.AWTChartComponentFactory;
import org.jzy3d.colors.Color;
import org.jzy3d.colors.ColorMapper;
import org.jzy3d.colors.colormaps.ColorMapRainbow;
import org.jzy3d.maths.Coord3d;
import org.jzy3d.maths.Range;
import org.jzy3d.maths.Rectangle;
import org.jzy3d.plot3d.builder.Builder;
import org.jzy3d.plot3d.builder.Mapper;
import org.jzy3d.plot3d.builder.concrete.OrthonormalGrid;
import org.jzy3d.plot3d.primitives.Point;
import org.jzy3d.plot3d.primitives.Polygon;
import org.jzy3d.plot3d.primitives.Shape;
import org.jzy3d.plot3d.rendering.canvas.Quality;

public class jzy3dPOC extends AbstractAnalysis {
    public static void main(String[] args) throws Exception {
    	Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
    	int screenWidth = (int) screenSize.getWidth();
    	int screenHeight = (int) screenSize.getHeight();
    	AnalysisLauncher.open(new jzy3dPOC(), new Rectangle(0, 0, screenWidth, screenHeight));
    }
    
    @Override
    public void init() {
        // Define a function to plot
        Mapper mapper = new Mapper() {
            public double f(double x, double y) {
                return x * Math.sin(x * y);
            }
        };

        // Define range and precision for the function to plot
        Range range = new Range(-2, 11);
        int steps = 80;
        
        List<Polygon> polygons = new ArrayList<Polygon>();
        Polygon polygon = new Polygon();
        polygon.add(new Point(new Coord3d(-1.0439, 0.16, 1)));
        polygon.add(new Point(new Coord3d(10.6715, 0.13, 1)));
        polygons.add(polygon);
        
        
        
        // Create the object to represent the function over the given range.
//        final Shape surface = Builder.buildOrthonormal(new OrthonormalGrid(range, steps, range, steps), mapper);
        final Shape surface = new Shape(polygons);
        surface.setColorMapper(new ColorMapper(new ColorMapRainbow(), surface.getBounds().getZmin(), surface.getBounds().getZmax(), new Color(1, 1, 1, .5f)));
        surface.setFaceDisplayed(true);
        surface.setWireframeDisplayed(false);

        // Create a chart
        chart = AWTChartComponentFactory.chart(Quality.Advanced, getCanvasType());
        chart.getScene().getGraph().add(surface);
    }
}
