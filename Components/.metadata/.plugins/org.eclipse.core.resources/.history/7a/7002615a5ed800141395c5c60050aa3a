package Model;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;

public class TransformedSpectra {

	ArrayList<BucketContainer> bContainers;
	Spectra oldSpectra;
	ArrayList<CorrelationRange> ranges;
	ArrayList<CorrelationPeak> allPeaks;
	double MIN_DISTANCE;

	public TransformedSpectra(ArrayList<BucketContainer> bContainers, double MIN_DISTANCE){
		this.MIN_DISTANCE = MIN_DISTANCE;
		this.bContainers = bContainers;
		this.allPeaks = new ArrayList<CorrelationPeak>();
		this.ranges = new ArrayList<CorrelationRange>();
		this.transformSpectrum();
	}

	public CorrelationRange getPeaksAt(double ppmRangeLow, double ppmRangeHigh){
		for(CorrelationRange cr : this.ranges){
			for(CorrelationPeak cp : cr.getPeaks()){
				if(cp.getPPMRef() >= ppmRangeLow && cp.getPPMRef() <= ppmRangeHigh){
					return cr;
				}
			}
		}

		return null;
	}

	private void transformSpectrum() throws IndexOutOfBoundsException{
		if(!bContainers.isEmpty()){
			if(!this.bContainers.isEmpty()){
				BucketContainer specZeroBucket = this.bContainers.get(0);
				for(PeakBucket pb1 : specZeroBucket.getBuckets()){
					CorrelationRange cr = new CorrelationRange();
					for(Peak p1 : pb1.getAllPeaks()){
						cr.addPeak(new CorrelationPeak(pb1.getSpectraID(), p1.getY(), p1.getX()));
						//				System.out.println("Peak added for spectra " + pb1.getSpectraID() + " Intensity " + p1.getY() + "PPM " + p1.getX());
						for(BucketContainer bc : this.bContainers){
							for(PeakBucket pb2: bc.getBuckets()){
								for(Peak p2 : pb2.getAllPeaks()){
									if((p2.getX() >= (p1.getX() - this.MIN_DISTANCE)) && (p2.getX() <= (p1.getX() + this.MIN_DISTANCE))){
										cr.addPeak(new CorrelationPeak(pb2.getSpectraID(), p2.getY(), p2.getX()));
										//								System.out.println("Peak added for spectra " + pb2.getSpectraID() + " Intensity " + p2.getY() + "PPM " + p2.getX());
									}
								}
							}
						}
					}
					if(!(cr.getPeaks().size() == 0)){
						cr = this.missingData(cr);
						System.out.println("Correlation range has " + cr.getPeaks().size() + " peaks");
						this.ranges.add(cr);
					}
				}
			}
		}else{
			System.out.println("Burp");
			throw new IndexOutOfBoundsException();
		}
	}

	private CorrelationRange missingData(CorrelationRange cr) {
		if(cr.getPeaks().size() != 64){
			int i=0;
			while(i != 64){
				boolean idCheck = false;
				for(CorrelationPeak cp : cr.getPeaks()){
					if(cp.getSpectrumID() == i){
						idCheck = true;
					}
				}

				if(!idCheck){
					cr.addPeak(new CorrelationPeak(i, 0.0, 0.0));
				}
				i++;
			}
		}
		return cr;
	}

	public static double round(double value, int places) {
		if (places < 0) throw new IllegalArgumentException();

		BigDecimal bd = new BigDecimal(value);
		bd = bd.setScale(places, RoundingMode.HALF_UP);
		return bd.doubleValue();
	}

	public ArrayList<CorrelationRange> getRanges(){
		return this.ranges;
	}
}
