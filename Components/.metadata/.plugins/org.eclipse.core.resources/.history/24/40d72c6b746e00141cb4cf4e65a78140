package CorrelationEngine;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

/**
 * This class represents Pearsons Product Moment Correlation Coefficient
 * It is what we will use to calculate correlations in individual snapshots and over 
 * multiple snapshots.
 * 
 * @author Davey
 */
public class PearsonsPMCC {

	public static void main(String[] args){
		new PearsonsPMCC();
	}
	
	double[] x, y, xsq, ysq, xy;
	int n_occurances;
	
	public PearsonsPMCC(){
		x = new double[500];
		y = new double[500];
		ysq = new double[500];
		xsq = new double[500];
		xy = new double[500];
		n_occurances = 0;
		
//		populateXY();
//		occurances();
//		System.out.println(calculateR());
	}
	
	private void populateXY() {
		try {
			FileReader fr = new FileReader("/Applications/XAMPP/xamppfiles/htdocs/NMR-Prototype-1/data.tsv");
			BufferedReader bfr = new BufferedReader(fr);
			
			/** Dismiss first line as it is column headers*/
			bfr.readLine();
			/** Initialize counter to begin populating x and y coordinates*/
			int i = 0;
			while(!bfr.readLine().equals(null)){
				String line = bfr.readLine();
				String[] tokens = line.split("\t");
				
				x[i] = Double.parseDouble(tokens[0]);
				y[i] = Double.parseDouble(tokens[1]);
			}
			
			bfr.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch(IOException e){
			e.printStackTrace();
		}
	}

	private void occurances(){
		if(x.length == y.length){
			this.n_occurances = x.length;
		}
	}
	
	private void calculateXSq(){
		int i=0;
		for(double n : x){
			xsq[i] = n*n;
			i++;
		}
	}
	
	private void calculateYSq(){
		int i=0;
		for(double n : y){
			ysq[i] = n*n;
			i++;
		}
	}
	
	private void calculateXY(){
		for(int i=0; i < x.length; i++){
			xy[i] = x[i]*y[i];
		}
	}
	
	private double sumXY(double[] xy){
		double sigmaXY = 1;
		calculateXY();
		for(double n : xy){
			sigmaXY = sigmaXY * n;
		}
		
		return sigmaXY;
	}
	
	private double sumX(double[] x){
		double sigmaX = 1;
		for(double n : x){
			sigmaX = sigmaX * n;
		}
		
		return sigmaX;
	}
	
	private double sumY(double[] y){
		double sigmaY = 1;
		for(double n : y){
			sigmaY = sigmaY * n;
		}
		
		return sigmaY;
	}
	
	private double sumXSq(double[] xsq){
		double sigmaX_AllSq = 1;
		calculateXSq();
		for(double n : xsq){
			sigmaX_AllSq = sigmaX_AllSq * n;
		}
		
		return sigmaX_AllSq;
	}
	
	private double sumYSq(double[] ysq){
		double sigmaY_AllSq = 1;
		calculateYSq();
		for(double n : ysq){
			sigmaY_AllSq = sigmaY_AllSq * n;
		}
		
		return sigmaY_AllSq;
	}
	
	private double calculate_Sxy(){
		double sigmaXY = sumXY(this.xy);
		double sigmaX = sumX(this.x);
		double sigmaY = sumY(this.y);
		
		return sigmaXY - ((sigmaX * sigmaY)/this.n_occurances);
	}
	
	private double calculate_Sxx(){
		double sigmaXSq = sumXSq(xsq);
		double sigmaX_AllSq = (sumX(x)*sumX(x));
		
		return (sigmaXSq - (sigmaX_AllSq / this.n_occurances));
	}
	
	private double calculate_Syy(){
		double sigmaYSq = sumYSq(ysq);
		double sigmaY_AllSq = (sumY(y)*sumY(y));
		
		return (sigmaYSq - (sigmaY_AllSq / this.n_occurances));
	}
	
	public double calculateR(){
		
		return ((calculate_Sxy()) / (Math.sqrt((calculate_Sxx() * calculate_Syy()))));
	}
}
