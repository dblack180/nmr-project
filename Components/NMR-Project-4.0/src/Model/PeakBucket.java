package Model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class PeakBucket {
	int spectraID;
	int ppmRangeHigh;
	int ppmRangeLow;
	int numPeaks;
	Map<Integer, Peak> bucket;
	
	public PeakBucket(int spectraID, int PPMRange){
		this.spectraID = spectraID;
		this.ppmRangeHigh = PPMRange;
		this.ppmRangeLow = PPMRange - 1;
		this.bucket = new HashMap<Integer, Peak>();
		this.numPeaks = 0;
	}
	
	public int getSpectraID(){
		return this.spectraID;
	}
	
	public int getPPMHigh(){
		return this.ppmRangeHigh;
	}
	
	public int getPPMLow(){
		return this.ppmRangeLow;
	}
	
	public void addPeak(int order, Peak peak){
		this.bucket.put(order, peak);
		this.numPeaks++;
	}
	
	private int sizeOfMap(){
		return bucket.size();
	}
	
	public ArrayList<Peak> getAllPeaks(){
		ArrayList<Peak> peaks = new ArrayList<Peak>();
		for(int i=0; i<this.sizeOfMap(); i++){
			peaks.add(bucket.get(i));
		}
		
		return peaks;
	}
	
	public int getNumPeaks(){
		return this.numPeaks;
	}
}
