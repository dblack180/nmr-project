package UtilsTests;

import static org.junit.Assert.*;

import java.io.File;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import Utils.PeakFileReader;

public class PeakFileReaderTests {

	PeakFileReader reader;
	File testDataDir;
	File failTestDataDir;

	public void initFixtures(){
		testDataDir = new File("testData/SortedPeakFiles/");
		failTestDataDir = new File("testData/SortedPeakFilesForFail/");
		reader = new PeakFileReader(testDataDir);
	}

	public void initFixturesNoReader(){
		testDataDir = new File("testData/SortedPeakFiles/");
		failTestDataDir = new File("testData/SortedPeakFilesForFail/");
	}

	@Test
	public void ifDirectoryTest(){
		initFixtures();
		assertTrue(testDataDir.exists() && testDataDir.isDirectory());
		assertEquals(reader.getNMRSpectra().getRanges().size(), 64);
	}

	@Test
	public void getAllTimeFrameIDsTest(){
		initFixtures();
		int j = 0;
		File[] checkedFiles = new File[64];
		for(int i=0; i<testDataDir.listFiles().length; i++){
			String formedPath = testDataDir.getAbsolutePath() + "/" + i + ".csv";
			for(File f : testDataDir.listFiles()){
				if(f.getAbsolutePath().equals(formedPath)){
					checkedFiles[j] = f;
					j++;
				}
			}
		}
		assertEquals(checkedFiles.length, 64);
		System.out.println("Timeframe ID Retrieval Test Complete..");
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void exceptionThrownForFalseDirectory() {
		initFixturesNoReader();
		reader = new PeakFileReader(failTestDataDir);
		System.out.println("Illegal argument for Data Set Test Complete..");
	}
	
	@Test
	public void roundingTest(){
		initFixtures();
		assertTrue(reader.round(1.0000, 0) == 1);
		assertTrue(reader.round(1.1000, 1) == 1.1);
		assertTrue(reader.round(1.1200, 2) == 1.12);
		assertTrue(reader.round(1.1230, 3) == 1.123);
		assertTrue(reader.round(1.1234, 4) == 1.1234);
		System.out.println("Rounding floating point values Test Complete..");
	}
}
