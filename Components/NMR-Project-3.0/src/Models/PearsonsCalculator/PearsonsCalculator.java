package Models.PearsonsCalculator;

import java.util.ArrayList;
import java.util.List;

import Model.CorrelationPeak;

public class PearsonsCalculator {
	List<Double> x;
	List<Double> y;
	List<Double> xSq;
	List<Double> ySq;
	List<Double> xy;
	List<CorrelationPeak> vectorOne;
	List<CorrelationPeak> vectorTwo;


	public PearsonsCalculator(ArrayList<CorrelationPeak> vectorOne, ArrayList<CorrelationPeak> vectorTwo){
		init();
		this.vectorOne = vectorOne;
		this.vectorTwo = vectorTwo;
		this.populateXY();
	}

	private void init(){
		this.x = new ArrayList<Double>();
		this.y = new ArrayList<Double>();
		this.xSq = new ArrayList<Double>();
		this.ySq = new ArrayList<Double>();
		this.xy = new ArrayList<Double>();
	}

	private void populateXY(){
		for(CorrelationPeak cp : vectorOne){
			this.x.add(cp.getIntensity());
		}
		for(CorrelationPeak cp : vectorTwo){
			this.y.add(cp.getIntensity());
		}
	}

	private double calculateMeanOfX(){
		double total = 0;
		for(Double x : this.x){
			total+=x;
		}

		double mean = total / (this.x.size());
		return mean;
	}

	private double calculateMeanOfY(){
		double total = 0;
		for(Double y : this.y){
			total+=y;
		}

		double mean = total / (this.y.size());
		return mean;
	}

	private double calculateSSx(){
		double sumX = 0;

		double xMean = this.calculateMeanOfX();
		for(Double x : this.x){
			sumX+=(x - xMean) * (x - xMean);
		}

		return sumX;
	}

	private double calculateSSy(){
		double sumY = 0;

		double yMean = this.calculateMeanOfY();
		for(Double y : this.y){
			sumY+=(y - yMean) * (y - yMean);
		}

		return sumY;
	}

	private double calculateXYCombined(){
		double sumXY = 0;
		double xMean = this.calculateMeanOfX();
		double yMean = this.calculateMeanOfY();

		for(int i=0; i<(this.x.size()); i++){
			sumXY+=((this.x.get(i) - xMean) *(this.y.get(i) - yMean)); 

		}

		return sumXY;
	}

	public double calculateR(){
		double r = (this.calculateXYCombined() / (Math.sqrt(this.calculateSSx() * this.calculateSSy())));

		if(this.calculateXYCombined() == 0 || (Math.sqrt(this.calculateSSx() * this.calculateSSy())) == 0){
			r = 0.0;
		}

		return r;
	}
}