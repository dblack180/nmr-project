package Utils;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class PeakCreator {

	String targetFile = "peaklist.xml";
	List<File> all_paths = new ArrayList<File>();
	List<File> visited = new ArrayList<File>();
	Queue<File> crawlQueue = new LinkedList<File>();
	File[] sortedPaths;

	public PeakCreator(File rootDir){
		crawlQueue.add(rootDir);
		this.DFS_FileSearch();
		File[] sortedFiles = this.sortPaths();
		this.printPaths(sortedFiles);
		this.writeSortedPeakFiles();
	}

	private void DFS_FileSearch(){
		while(!crawlQueue.isEmpty()){
			File curr = crawlQueue.poll();
			visited.add(curr);
			if(curr.getName().equals(targetFile)){
				all_paths.add(curr);
			}

			if(curr.isDirectory()){
				for(File f : curr.listFiles()){
					if(!visited.contains(f) && !crawlQueue.contains(f)){
						crawlQueue.add(f);
					}
				}
			}
		}
	}

	private File[] sortPaths(){
		sortedPaths = new File[all_paths.size()];
		int i = 0;

		for(File f : all_paths){
			sortedPaths[i] = f;
			i++;
		}

		Arrays.sort(sortedPaths, new Comparator<File>() {
			@Override
			public int compare(File o1, File o2) {
				int n1 = getIndex(o1.getAbsolutePath());
				int n2 = getIndex(o2.getAbsolutePath());
				return n1 - n2;
			}

			private int getIndex(String name) {
				String[] tokens = name.split("/");
				int tokensSize = tokens.length;

				return Integer.parseInt(tokens[tokensSize-4]); 
			}

		});

		return sortedPaths;
	}

	private void printPaths(File[] sortedFiles){
		for(File f : sortedFiles){
			System.out.println("File " + f.getAbsolutePath());
		}
	}

	public File[] getSortedPaths(){
		return this.sortedPaths;
	}

	private void writeSortedPeakFiles(){
		File peakDir = new File("data/SortedPeakFiles/");

		for(File f : peakDir.listFiles()){
			f.delete();
		}

		for(File f : sortedPaths){
			try {
				String line = "";
				String fileNum = String.valueOf(getIndex(f.getAbsolutePath()) - 1);
				FileWriter fw = new FileWriter(peakDir + "/" + fileNum + ".csv");
				BufferedWriter bw = new BufferedWriter(fw);

//				System.out.println("Writing file to " + peakDir + fileNum + ".csv");
				
				try {
					File xmlFile = f;
					System.out.println("File being parsed: " + xmlFile.getAbsolutePath());
					DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
					DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
					Document doc = dBuilder.parse(xmlFile);

					doc.getDocumentElement().normalize();

					NodeList nList = doc.getElementsByTagName("Peak1D");

					for(int i=0; i < nList.getLength(); i++){

						Node n = nList.item(i);

						if(n.getNodeType() == Node.ELEMENT_NODE){
							Element element = (Element) n;

							double ppm = Double.parseDouble(element.getAttribute("F1"));
							double intensity = Double.parseDouble(element.getAttribute("intensity"));
							int spectra = getIndex(f.getAbsolutePath());
							bw.write(ppm + "," + intensity + "," + spectra + "\n");
						}
					}
					
				} catch (ParserConfigurationException e) {
					e.printStackTrace();
				} catch (SAXException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}

				bw.close();

			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	private int getIndex(String name) {
		String[] tokens = name.split("/");
		int tokensSize = tokens.length;

		return Integer.parseInt(tokens[tokensSize-4]); 
	}
}

