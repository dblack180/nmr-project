package Utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.nio.file.DirectoryIteratorException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;

import Model.Spectra;
import Model.Peak;
import Model.PeakRange;

public class PeakFileReader {

	Spectra nmrSpectra;
	String path = null;

	public PeakFileReader(){
		this.initStructures();
		this.parseCSVS();
	}

	//	public PeakFileReader(File chosenDir) {
	//		if(isWorthy(chosenDir)){
	//			this.initStructures();
	//			path = chosenDir.getAbsolutePath();
	//			this.parseCSVS();
	//		}else{
	//			
	//		}
	//	}
	//
	//	private boolean isWorthy(File chosenDir){
	//		boolean worthy = false;
	//		
	//		if(chosenDir.isDirectory()){
	//			for(String f : chosenDir.list()){
	//				if(f.equals("0.csv")){
	//					worthy = true;
	//				}
	//			}
	//		}
	//		
	//		return worthy;
	//	}

	public void parseCSVS(){
		Path nmrDir;

		nmrDir = Paths.get("data/SortedPeakFiles/");
		System.out.println("loading data set from data/SortedPeakFiles/");

		try (DirectoryStream<Path> dirStream = Files.newDirectoryStream(nmrDir)) {
			for (Path file: dirStream) {
				PeakRange range = new PeakRange(new ArrayList<Peak>(), this.getTimeframeID(file.getFileName()));
				BufferedReader br = new BufferedReader(new FileReader(nmrDir.toString() + "/" + file.getFileName().toString()));
				String line = "";
				while((line = br.readLine()) != null){
					String[] tokens = line.split(",");
					range.addPeak(new Peak(
							round((Float.parseFloat(tokens[0])),3),
							(Float.parseFloat(tokens[1])),
							Float.parseFloat(tokens[2]
									)));
				}

				this.nmrSpectra.addRange(range);
			}
		} catch (IOException | DirectoryIteratorException e) {
			e.printStackTrace();
		}
	}

	public static double round(double value, int places) {
		if (places < 0) throw new IllegalArgumentException();

		BigDecimal bd = new BigDecimal(value);
		bd = bd.setScale(places, RoundingMode.HALF_UP);
		return bd.doubleValue();
	}

	public int getTimeframeID(Path filename){
		String fileStr = filename.toString();
		String[] tokens = fileStr.split("");
		int ID = 0;

		if(tokens.length == 5){
			ID = Integer.parseInt(tokens[0]);
		}else if(tokens.length == 6){
			ID = Integer.parseInt(tokens[0] + tokens[1]);
		}

		return ID;
	}

	public Spectra getNMRSpectra(){
		return this.nmrSpectra;
	}

	public void initStructures(){
		this.nmrSpectra = new Spectra(new ArrayList<PeakRange>());
	}
}
