package Model.CorrelationEngine;

import javax.swing.JPanel;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYAreaRenderer;

import Model.CorrelationPeak;
import Model.CorrelationRange;
import Model.TransformedSpectra;
import Model.TransformedSpectraTwo;

public class SubtrateConcentrationGraph {

	CorrelationEngineTwo CE;
	TransformedSpectraTwo TS;
	ChartPanel first = null;
	ChartPanel second = null;
	int fstPPM = 0;
	int sndPPM = 0;
	
	public SubtrateConcentrationGraph(CorrelationEngineTwo CE){
		this.CE = CE;
		this.TS = CE.getTSpectra();
	}

	@SuppressWarnings("unused")
	public void initialiseSubtrateGraphs(JPanel panel1, JPanel panel2){
		first = new ChartPanel(this.createFirstSubtrate());
		first.setSize(panel1.getWidth(), panel1.getHeight());
		first.setVisible(true);
		second = new ChartPanel(this.createSecondSubtrate());
		second.setSize(panel2.getWidth(), panel2.getHeight());
		second.setVisible(true);
	}
	
	public void initialiseSubtrateGraphs(JPanel panel1, JPanel panel2, int firstPpmRange, int secondPpmRange){
		first = new ChartPanel(this.createFirstSubtrate(firstPpmRange));
		first.setSize(panel1.getWidth(), panel1.getHeight());
		first.setVisible(true);
		second = new ChartPanel(this.createSecondSubtrate(secondPpmRange));
		second.setSize(panel2.getWidth(), panel2.getHeight());
		second.setVisible(true);
	}
	
	public ChartPanel getFirst(){
		return this.first;
	}
	
	public ChartPanel getSecond(){
		return this.second;
	}
	
	private JFreeChart createFirstSubtrate(){
		XYDataset dataset = this.firstSubtrateDataSet();
		final JFreeChart chart = ChartFactory.createXYLineChart(             
				"PPM Range 0 - 1", 
				"PPM",              
				"Intensity",              
				dataset);

		XYPlot plot = (XYPlot) chart.getXYPlot();

//		plot.setRenderer(new XYAreaRenderer());
		return chart;
	}
	
	private JFreeChart createSecondSubtrate(){
		XYDataset dataset = this.secondSubtrateDataSet();
		final JFreeChart chart = ChartFactory.createXYLineChart(             
				"PPM Range 1 - 2", 
				"PPM",              
				"Intensity",              
				dataset);

		XYPlot plot = (XYPlot) chart.getXYPlot();

//		plot.setRenderer(new XYAreaRenderer());
		return chart;
	}
	
	private JFreeChart createFirstSubtrate(int ppmRange){
		XYDataset dataset = this.firstSubtrateDataSet(ppmRange);
		int fstSuc = this.fstPPM+1;
		final JFreeChart chart = ChartFactory.createXYLineChart(             
				"PPM Range " + this.fstPPM + " - " + fstSuc, 
				"PPM",              
				"Intensity",              
				dataset);

		XYPlot plot = (XYPlot) chart.getXYPlot();

//		plot.setRenderer(new XYAreaRenderer());
		return chart;
	}
	
	private JFreeChart createSecondSubtrate(int ppmRange){
		XYDataset dataset = this.secondSubtrateDataSet(ppmRange);
		int sndSuc = this.sndPPM+1;
		final JFreeChart chart = ChartFactory.createXYLineChart(             
				"PPM Range " + this.sndPPM + " - " + sndSuc, 
				"PPM",              
				"Intensity",              
				dataset);

		XYPlot plot = (XYPlot) chart.getXYPlot();

//		plot.setRenderer(new XYAreaRenderer());
		return chart;
	}
	
	private XYDataset firstSubtrateDataSet(){
		CorrelationRange CR0 = this.TS.getRanges().get(0);
		final XYSeriesCollection dataset = new XYSeriesCollection();
		final XYSeries sbstr = new XYSeries(CR0.getPPMRange());
		for(CorrelationPeak cp : CR0.getPeaks()){
			sbstr.add(cp.getSpectrumID(), cp.getIntensity());
		}
		
		dataset.addSeries(sbstr);
		return dataset;
	}
	
	private XYDataset secondSubtrateDataSet(){
		CorrelationRange CR1 = this.TS.getRanges().get(1);
		final XYSeriesCollection dataset = new XYSeriesCollection();
		final XYSeries sbstr = new XYSeries(CR1.getPPMRange());
		for(CorrelationPeak cp : CR1.getPeaks()){
			sbstr.add(cp.getSpectrumID(), cp.getIntensity());
		}
		
		dataset.addSeries(sbstr);
		return dataset;
	}
	
	private XYDataset firstSubtrateDataSet(int ppmRange){
		this.fstPPM = ppmRange;
		CorrelationRange CR = this.TS.getRanges().get(ppmRange);
		final XYSeriesCollection dataset = new XYSeriesCollection();
		final XYSeries sbstr = new XYSeries(CR.getPPMRange());
		for(CorrelationPeak cp : CR.getPeaks()){
			sbstr.add(cp.getSpectrumID(), cp.getIntensity());
		}
		
		dataset.addSeries(sbstr);
		return dataset;
	}
	
	private XYDataset secondSubtrateDataSet(int ppmRange){
		this.sndPPM = ppmRange;
		CorrelationRange CR = this.TS.getRanges().get(ppmRange);
		final XYSeriesCollection dataset = new XYSeriesCollection();
		final XYSeries sbstr = new XYSeries(CR.getPPMRange());
		for(CorrelationPeak cp : CR.getPeaks()){
			sbstr.add(cp.getSpectrumID(), cp.getIntensity());
		}
		
		dataset.addSeries(sbstr);
		return dataset;
	}
}
