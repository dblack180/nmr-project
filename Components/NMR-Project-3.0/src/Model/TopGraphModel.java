package Model;

import java.io.File;

import javax.swing.JPanel;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYAreaRenderer;

import Utils.PeakFileReader;
import View.MainWindow;

public class TopGraphModel {

	Spectra NMRSpectra;
	String perspective;
	boolean wireframe;
	double lowerRegion = 0.0;
	double higherRegion = 0.0;
	PeakFileReader pfr;
	
	public TopGraphModel(String perspective, boolean wireframe, File chosenDir){
		
		pfr = new PeakFileReader();
		this.setSpectra(pfr.getNMRSpectra());
		this.perspective = perspective;
		this.wireframe = wireframe;
	}

	public TopGraphModel(double lowerRegion, double higherRegion, File chosenDir) {
		this.lowerRegion = lowerRegion;
		this.higherRegion = higherRegion;
		pfr = new PeakFileReader();
		this.setSpectra(pfr.getNMRSpectra());
		this.perspective = "Left";
		this.wireframe = false;
	}

	public double getLowerRegion(){
		return this.lowerRegion;
	}

	public double getHigherRegion(){
		return this.higherRegion;
	}

	private void setSpectra(Spectra newSpectra){
		this.NMRSpectra = newSpectra;
	}

	public Spectra getSpectra(){
		return this.NMRSpectra;
	}

	private XYDataset createNMRDataset(){

		double i = 0.0;

		if(this.lowerRegion != 0.0 && this.higherRegion != 0.0){
			final XYSeriesCollection dataset = new XYSeriesCollection();
			for(PeakRange r : getSpectra().sortedRange()){
				final XYSeries series = new XYSeries("Spectrum " +  r.timeframeID);
				for(Peak p : r.getRange()){
					if(p.getX() >= this.lowerRegion && p.getX() <= this.higherRegion){
						if(this.perspective.equals("Left")){
							series.add((double) p.getX() + i,(double) p.getY() + i);
						}else if(this.perspective.equals("Right")){
							series.add((double) p.getX() - i,(double) p.getY() + i);
						}
					}
				}

				i+=0.01;
				dataset.addSeries(series);
			}

			return dataset;

		}else{
			final XYSeriesCollection dataset = new XYSeriesCollection();
			for(PeakRange r : getSpectra().sortedRange()){
				final XYSeries series = new XYSeries("Spectrum " +  r.timeframeID);
				for(Peak p : r.getRange()){
					if(this.perspective.equals("Left")){
						series.add((double) p.getX() + i,(double) p.getY() + i);
					}else if(this.perspective.equals("Right")){
						series.add((double) p.getX() - i,(double) p.getY() + i);
					}
				}

				i+=0.01;
				dataset.addSeries(series);
			}

			return dataset;
		}
	}

	public String getPerspective(){
		return this.perspective;
	}

	public ChartPanel getTopGraph(JPanel container){
		ChartPanel chart = new ChartPanel(this.createGraph());
		chart.setSize(container.getWidth(), container.getHeight());
		chart.setVisible(true);
		return chart;
	}

	private JFreeChart createGraph(){

		XYDataset dataset = this.createNMRDataset();
		final JFreeChart chart = ChartFactory.createXYLineChart(             
				"NMR Hybrid Display", 
				"PPM",              
				"Intensity",              
				dataset);

		XYPlot plot = (XYPlot) chart.getXYPlot();
		NumberAxis domain = (NumberAxis) plot.getDomainAxis();
		domain.setRange(this.NMRSpectra.getLowestPPM(), this.NMRSpectra.getHighestPPM());
		domain.setInverted(true);
		if(wireframe == true){

		}else{
			plot.setRenderer(new XYAreaRenderer());
		}
		return chart;
	}

}
