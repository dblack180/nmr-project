package View;

import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.ConcurrentModificationException;
import java.util.Hashtable;
import java.util.List;
import java.util.Observable;
import java.util.Observer;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.swing.Icon;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import org.jfree.chart.ChartPanel;

import Model.Quantiser;
import Model.TopGraphModel;
import Model.CorrelationEngine.CorrelationEngineTwo;
import Model.CorrelationEngine.CorrelationGraph;
import Model.CorrelationEngine.SubtrateConcentrationGraphTwo;
import Models.PearsonsCalculator.PearsonsCoefficient;
import Utils.PeakCreator;

/**
 *
 * @author Davey
 */
public class MainWindow extends javax.swing.JFrame implements Observer, Runnable {

	private ArrayList<PearsonsCoefficient> concList;
	/**
	 * 
	 */
	private static final long serialVersionUID = 5422048462448123150L;

	/**
	 * Creates new form MainWindow
	 */
	public MainWindow() {
		executor = Executors.newFixedThreadPool(4);
		initComponents();
		populateComponents();
		this.wireframeView = false;
	}

	private void initSetChooser(){
		fileChooser = new JFileChooser(); 
		fileChooser.setCurrentDirectory(new java.io.File("."));
		fileChooser.setDialogTitle(choosertitle);
		fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		fileChooser.setAcceptAllFileFilterUsed(false);
		this.chosenDir = new File("/");
		if (fileChooser.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) { 
			this.chosenDir = fileChooser.getSelectedFile();
			System.out.println(this.chosenDir.getAbsolutePath());
		}
		
		new PeakCreator(chosenDir);
	}
	
	private void initComponents() {
		this.initSetChooser();
		subRegioned = false;
		quantiser = new Quantiser(0);
		buttonGroup1 = new javax.swing.ButtonGroup();
		jFrame1 = new javax.swing.JFrame();
		jLabel1 = new javax.swing.JLabel();
		jPanel1 = new javax.swing.JPanel();
		jSeparator1 = new javax.swing.JSeparator();
		jPanel2 = new javax.swing.JPanel();
		jButton1 = new javax.swing.JButton();
		jButton2 = new javax.swing.JButton();
		jButton3 = new javax.swing.JButton();
		jButton4 = new javax.swing.JButton();
		jPanel3 = new javax.swing.JPanel();
		jRadioButton1 = new javax.swing.JRadioButton();
		jRadioButton2 = new javax.swing.JRadioButton();
		list1 = new java.awt.List();
		jPanel4 = new javax.swing.JPanel();
		jPanel5 = new javax.swing.JPanel();
		jLabel2 = new javax.swing.JLabel();
		jScrollPane1 = new javax.swing.JScrollPane();
		jTextArea1 = new javax.swing.JTextArea();
		jLabel3 = new javax.swing.JLabel();
		concList = new ArrayList<PearsonsCoefficient>();
		quantity = new JSlider();

		javax.swing.GroupLayout jFrame1Layout = new javax.swing.GroupLayout(jFrame1.getContentPane());
		jFrame1.getContentPane().setLayout(jFrame1Layout);
		jFrame1Layout.setHorizontalGroup(
				jFrame1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGap(0, 400, Short.MAX_VALUE)
				);
		jFrame1Layout.setVerticalGroup(
				jFrame1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGap(0, 300, Short.MAX_VALUE)
				);

		setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
		setBackground(new java.awt.Color(255, 255, 255));
		setSize(new java.awt.Dimension(1024, 800));

		jLabel1.setFont(new java.awt.Font("Helvetica", 1, 36)); // NOI18N
		jLabel1.setForeground(new java.awt.Color(51, 153, 0));
		jLabel1.setText("Strathclyde NMR");

		javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
		jPanel1.setLayout(jPanel1Layout);
		jPanel1Layout.setHorizontalGroup(
				jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGap(0, 1343, Short.MAX_VALUE)
				);
		jPanel1Layout.setVerticalGroup(
				jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGap(0, 465, Short.MAX_VALUE)
				);

		jButton1.setText("Reset");
		jButton1.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				jButton1ActionPerformed(evt);
			}
		});

		jButton2.setText("3D View");
		jButton2.addActionListener(new java.awt.event.ActionListener(){
			public void actionPerformed(ActionEvent e) {
				executor.execute(new PyRunner());
			}

		});

		jButton3.setText("Sub-Region");
		jButton3.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				String s = (String)JOptionPane.showInputDialog(
						"Enter range to inspect "
								+ "\n ie (2.0 - 5.0)");
				plotSubRegion(s);
			}
		});

		jButton4.setText("Left/Right");
		jButton4.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				jButton4ActionPerformed(evt);
			}
		});

		javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
		jPanel2.setLayout(jPanel2Layout);
		jPanel2Layout.setHorizontalGroup(
				jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGroup(jPanel2Layout.createSequentialGroup()
						.addContainerGap()
						.addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
								.addComponent(jButton3, javax.swing.GroupLayout.DEFAULT_SIZE, 125, Short.MAX_VALUE)
								.addComponent(jButton1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
								.addGap(18, 18, Short.MAX_VALUE)
								.addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
										.addComponent(jButton2, javax.swing.GroupLayout.DEFAULT_SIZE, 117, Short.MAX_VALUE)
										.addComponent(jButton4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
										.addContainerGap())
				);
		jPanel2Layout.setVerticalGroup(
				jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGroup(jPanel2Layout.createSequentialGroup()
						.addContainerGap()
						.addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
								.addComponent(jButton1, javax.swing.GroupLayout.DEFAULT_SIZE, 39, Short.MAX_VALUE)
								.addComponent(jButton2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
								.addGap(12, 12, 12)
								.addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
										.addComponent(jButton4, javax.swing.GroupLayout.DEFAULT_SIZE, 40, Short.MAX_VALUE)
										.addComponent(jButton3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
										.addContainerGap())
				);

		javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
		jPanel4.setLayout(jPanel4Layout);
		jPanel4Layout.setHorizontalGroup(
				jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGap(0, 637, Short.MAX_VALUE)
				);
		jPanel4Layout.setVerticalGroup(
				jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGap(0, 393, Short.MAX_VALUE)
				);

		jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
		jLabel2.setText("Notes");

		jTextArea1.setColumns(20);
		jTextArea1.setRows(5);
		jScrollPane1.setViewportView(jTextArea1);

		javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
		jPanel5.setLayout(jPanel5Layout);
		jPanel5Layout.setHorizontalGroup(
				jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGap(0, 637, Short.MAX_VALUE)
				);
		jPanel5Layout.setVerticalGroup(
				jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGap(0, 393, Short.MAX_VALUE)
				);

		jLabel3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
		jLabel3.setText("Subtrate Concentration");

		javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
		getContentPane().setLayout(layout);
		layout.setHorizontalGroup(
				layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGroup(layout.createSequentialGroup()
						.addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
								.addComponent(jSeparator1)
								.addGroup(layout.createSequentialGroup()
										.addContainerGap()
										.addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
												.addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
														.addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
																.addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
																.addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
																.addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
																		.addGroup(layout.createSequentialGroup()
																				.addGap(226, 226, 226)
																				.addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
																				.addGroup(layout.createSequentialGroup()
																						.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 182, Short.MAX_VALUE)
																						.addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
																						.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
																						.addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
																						.addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
																								.addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
																										.addComponent(jLabel1, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
																										.addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
																										.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
																										.addComponent(list1, javax.swing.GroupLayout.PREFERRED_SIZE, 242, javax.swing.GroupLayout.PREFERRED_SIZE))))
																										.addComponent(quantity, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
																										.addContainerGap())
				);
		layout.setVerticalGroup(
				layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGroup(layout.createSequentialGroup()
						.addContainerGap()
						.addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
						.addGap(18, 18, 18)
						.addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
								.addComponent(list1, javax.swing.GroupLayout.DEFAULT_SIZE, 465, Short.MAX_VALUE)
								.addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
								.addGap(18, 18, 18)
								.addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 4, javax.swing.GroupLayout.PREFERRED_SIZE)
								.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
								.addComponent(quantity, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
								.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
								.addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
										.addGroup(layout.createSequentialGroup()
												.addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
														.addGroup(layout.createSequentialGroup()
																.addGap(0, 0, Short.MAX_VALUE)
																.addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
																.addGap(18, 18, 18)
																.addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
																.addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
																.addContainerGap())
																.addGroup(layout.createSequentialGroup()
																		.addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
																		.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
																		.addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
				);



		quantity.setPaintTicks(true);
//		quantity.setPaintLabels(true);
		quantity.setSnapToTicks(true);
		quantity.setMajorTickSpacing(20);
		quantity.setLabelTable(this.getSliderLabels());
		quantity.setValue(0);
		quantity.addChangeListener(new ChangeListener(){

			@Override
			public void stateChanged(ChangeEvent e) {
				JSlider source = (JSlider)e.getSource();
				int places = (source.getValue() / 20);
				if(!(places == quantiser.getPlaces())){
					quantiser.setPlaces(places);
					System.out.println("Set quantiser value to " + quantiser.getPlaces());
					CE.notifyQuantiser(quantiser);
					CE.calculatePearsonsCoefficient();
				}
			}

		});
		jLabel3.hide();
		pack();
	}                

	private Hashtable<Integer, JLabel> getSliderLabels(){
		Hashtable<Integer, JLabel> LABELS = new Hashtable<>();

		for(int i = 0; i <= 4; ++i)
		{
			LABELS.put(i, new JLabel(" " + i + " DECIMAL PLACES"));
		}

		return LABELS;
	}

	private void plotSubRegion(String region){
		String subRegion = region.replace(" ", "");

		String[] regions = subRegion.split("-");
		double lowerRegion = Double.parseDouble(regions[0]);
		double higherRegion = Double.parseDouble(regions[1]);

		tgm = new TopGraphModel(lowerRegion, higherRegion, this.chosenDir);
		ChartPanel p = tgm.getTopGraph(jPanel1);
		jPanel1.removeAll();
		jPanel1.add(p);
		jPanel1.repaint();
		this.subRegioned = true;
	}

	private void generateSubGraph(String selectedItem){
		System.out.println("Viewing correlation: " + selectedItem);
		if(!this.subRegioned){
			this.reset();
		}
		System.out.println("Checking for matches.");
		for(PearsonsCoefficient pc : this.concList){
			if(pc.getEDCode().equals(selectedItem)){
				System.out.println("Found match: " + pc.getEDCode() + " - " + selectedItem);
				CorrelationGraph CG;
				if(this.subRegioned){
					CG = new CorrelationGraph(tgm.getSpectra(), pc.getFirstVector(), pc.getSecondVector(), this.tgm.getLowerRegion(), this.tgm.getHigherRegion());
				}else{
					CG = new CorrelationGraph(tgm.getSpectra(), pc.getFirstVector(), pc.getSecondVector());
				}
				ChartPanel p = CG.getCorrelationGraph(jPanel1);
				jPanel1.removeAll();
				jPanel1.add(p);
				jPanel1.repaint();


				System.out.println("first vector range: " + (int) pc.getFirstVector().getPPMRange() + " second vector range: " + (int)pc.getSecondVector().getPPMRange());
				scg.initialiseSubtrateGraphsTwo(jPanel4, jPanel5, pc.getFirstVector().getPPMLow(), pc.getFirstVector().getPPMHigh(), pc.getSecondVector().getPPMLow(), pc.getSecondVector().getPPMHigh());
				executor.execute(new VisRunner(jPanel4, scg.getFirst()));
				executor.execute(new VisRunner(jPanel5, scg.getSecond()));
			}
		}
	}

	private class VisRunner implements Runnable{

		JPanel panel = null;
		ChartPanel graph = null;

		public VisRunner(JPanel panel, ChartPanel graph){
			this.panel = panel;
			this.graph = graph;
		}

		@Override
		public void run() {
			panel.removeAll();
			panel.add(graph);
			panel.repaint();
		}


	}

	private class PyRunner implements Runnable{

		@Override
		public void run() {
			System.out.println("Starting 3D Visualisation");
			String pythonScriptPath = "/Users/Davey/Documents/Uni/Individual Project/System/PythonPOC/matplotlib3dpoc.py";
			String[] cmd = new String[2];
			cmd[0] = "python2.6";
			cmd[1] = pythonScriptPath;

			Runtime rt = Runtime.getRuntime();
			Process pr = null;
			try {
				pr = rt.exec(cmd);
				BufferedReader bfr = new BufferedReader(new InputStreamReader(pr.getInputStream()));
				String line = "";
				while((line = bfr.readLine()) != null) {
					System.out.println(line);
				}
				System.out.println("Finished Python Vis");
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

	}

	private void populateComponents(){
		list1.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent evt) {
				java.awt.List list = (java.awt.List) evt.getSource();
				generateSubGraph(list.getSelectedItem());
			}
		});

		tgm = new TopGraphModel("Left", this.wireframeView, this.chosenDir);
		ChartPanel p = tgm.getTopGraph(jPanel1);

		executor.execute(new VisRunner(jPanel1, p));

		CE = new CorrelationEngineTwo(tgm.getSpectra(), quantiser);
		CE.addObserver(this);
		CE.calculatePearsonsCoefficient();

		scg = new SubtrateConcentrationGraphTwo(CE);
		scg.initialiseSubtrateGraphsTwo(jPanel4, jPanel5);
		
		executor.execute(new VisRunner(jPanel4, scg.getFirst()));
		executor.execute(new VisRunner(jPanel5, scg.getSecond()));
	}

	public synchronized void updateRankings(List<PearsonsCoefficient> newRankings){
		try{
			list1.removeAll();
			for(PearsonsCoefficient ed : newRankings){
				this.concList.add(ed);
				list1.add(ed.getEDCode());
				list1.repaint();
			}
		}catch(ConcurrentModificationException | NullPointerException e){
			e.printStackTrace();
		}
	}

	private void changeViewingPerspective(){
		if(tgm.getPerspective().equals("Left")){
			tgm = new TopGraphModel("Right", this.wireframeView, this.chosenDir);
		}else if(tgm.getPerspective().equals("Right")){
			tgm = new TopGraphModel("Left", this.wireframeView, this.chosenDir);
		}
		ChartPanel p = tgm.getTopGraph(jPanel1);
		jPanel1.removeAll();
		jPanel1.add(p);
		jPanel1.repaint();
	}

	private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {                                         
				if(this.wireframeView == true){
					this.wireframeView = false;
				}else{
					this.wireframeView = true;
				}
				tgm = new TopGraphModel(tgm.getPerspective(), this.wireframeView, this.chosenDir);
				ChartPanel p = tgm.getTopGraph(jPanel1);
				jPanel1.removeAll();
				jPanel1.add(p);
				jPanel1.repaint();
	}                                        

	private void reset(){
		this.subRegioned = false;
		tgm = new TopGraphModel(tgm.getPerspective(), this.wireframeView, this.chosenDir);
		ChartPanel p = tgm.getTopGraph(jPanel1);
		jPanel1.removeAll();
		jPanel1.add(p);
		jPanel1.repaint();
	}

	private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {                                         
		tgm = new TopGraphModel(tgm.getPerspective(), this.wireframeView, this.chosenDir);
		ChartPanel p = tgm.getTopGraph(jPanel1);
		jPanel1.removeAll();
		jPanel1.add(p);
		jPanel1.repaint();
	}

	private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {                                         
		this.changeViewingPerspective();
	}                                        

	public static void main(String args[]) {
		ExecutorService executor = Executors.newFixedThreadPool(1);

		try {
			for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
				if ("Nimbus".equals(info.getName())) {
					javax.swing.UIManager.setLookAndFeel(info.getClassName());
					break;
				}
			}
		} catch (ClassNotFoundException ex) {
			java.util.logging.Logger.getLogger(MainWindow.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
		} catch (InstantiationException ex) {
			java.util.logging.Logger.getLogger(MainWindow.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
		} catch (IllegalAccessException ex) {
			java.util.logging.Logger.getLogger(MainWindow.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
		} catch (javax.swing.UnsupportedLookAndFeelException ex) {
			java.util.logging.Logger.getLogger(MainWindow.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
		}

		executor.execute(new MainWindow());
	}
	private File chosenDir;
	private String choosertitle;
	private JFileChooser fileChooser;
	private boolean subRegioned;
	private Quantiser quantiser;
	private JSlider quantity; 
	public CorrelationEngineTwo CE;
	private ExecutorService executor;
	private boolean wireframeView;
	private TopGraphModel tgm;
	private SubtrateConcentrationGraphTwo scg;
	private javax.swing.ButtonGroup buttonGroup1;
	private javax.swing.JButton jButton1;
	private javax.swing.JButton jButton2;
	private javax.swing.JButton jButton3;
	private javax.swing.JButton jButton4;
	private javax.swing.JFrame jFrame1;
	private javax.swing.JLabel jLabel1;
	private javax.swing.JLabel jLabel2;
	private javax.swing.JLabel jLabel3;
	private javax.swing.JPanel jPanel1;
	private javax.swing.JPanel jPanel2;
	private javax.swing.JPanel jPanel3;
	private javax.swing.JPanel jPanel4;
	private javax.swing.JPanel jPanel5;
	private javax.swing.JRadioButton jRadioButton1;
	private javax.swing.JRadioButton jRadioButton2;
	private javax.swing.JScrollPane jScrollPane1;
	private javax.swing.JSeparator jSeparator1;
	private javax.swing.JTextArea jTextArea1;
	private java.awt.List list1;

	@Override
	public void update(Observable o, Object arg) {
		CorrelationEngineTwo CE_Tmp = (CorrelationEngineTwo) o;
		List<PearsonsCoefficient> rankings = CE_Tmp.getRankings();
		this.updateRankings(rankings);
	}

	@Override
	public void run() {
		this.setVisible(true);
	}


}
