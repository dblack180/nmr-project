package Model;

import java.util.ArrayList;
import java.util.List;

public class Spectra {

	List<PeakRange> spectra;
	double highestPPM = Double.MIN_VALUE;
	double lowestPPM = Double.MAX_VALUE;
	
	public Spectra(List<PeakRange> spectra){
		this.spectra = spectra;
	}

	public void addRange(PeakRange new_range){
		this.spectra.add(new_range);
		if(new_range.getHighestPPM() > this.highestPPM){
			this.highestPPM = new_range.getHighestPPM();
		}else if(new_range.getLowestPPM() < this.lowestPPM){
			this.lowestPPM = new_range.getLowestPPM();
		}
	}

	public void removeRange(int index){
		this.spectra.remove(index);
	}

	public List<PeakRange> getRanges(){
		return this.spectra;
	}

	public List<PeakRange> sortedRange(){
		int rangeSize = this.spectra.size();
		int i = 0;
		List<PeakRange> sortedRanges = new ArrayList<PeakRange>();

		while(i < rangeSize){
			for(PeakRange r : this.spectra){
				if(r.getID() == i){
					sortedRanges.add(r);
					i++;
				}
			}
		}

		return sortedRanges;
	}
	
	public double getLowestPPM(){
		return this.lowestPPM;
	}
	
	public double getHighestPPM(){
		return this.highestPPM;
	}
}
