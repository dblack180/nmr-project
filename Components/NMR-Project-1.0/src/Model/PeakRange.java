package Model;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;

public class PeakRange {

	List<Peak> range;
	int timeframeID;
	double highestPPM = Double.MIN_VALUE;
	double lowestPPM = Double.MAX_VALUE;

	public PeakRange(List<Peak> range, int ID){
		this.range = range;
		this.timeframeID = ID;
	}

	public void addPeak(Peak peak){
		if(peak.getY() != 4.9E-324){
			range.add(peak);
			if(peak.getX() > highestPPM){
				this.highestPPM = peak.getX();
			}else if(peak.getX() < lowestPPM){
				this.lowestPPM = peak.getX();
			}
		}
	}

	public void removePeak(Peak peak){
		range.remove(peak);
	}

	public void removePeak(int index){
		range.remove(index);
	}

	public Peak getPeakAt(int index){
		return this.range.get(index);
	}

	public ArrayList<Peak> getPeaksAt(int ppm){
		ArrayList<Peak> peaks = new ArrayList<Peak>();

		for(Peak p : this.range){
			if(((int) p.getX()) == ppm){
				peaks.add(p);
			}
		}

		return peaks;
	}

	public ArrayList<Peak> getPeaksAt(double ppm, int places){
		ArrayList<Peak> peaks = new ArrayList<Peak>();

		for(Peak p : this.range){
			if((round(p.getX(), places)) == ppm){
				peaks.add(p);
			}
		}

		return peaks;
	}

	public static double round(double value, int places) {
		if (places < 0) throw new IllegalArgumentException();

		BigDecimal bd = new BigDecimal(value);
		bd = bd.setScale(places, RoundingMode.HALF_UP);
		return bd.doubleValue();
	}

	private List<Peak> reversePPM(){
		List<Peak> newRange = new ArrayList<Peak>();
		for(int i=10; i>=0; i--){
			for(Peak p : this.getPeaksAt(i)){
				newRange.add(p);
			}
		}

		return newRange;
	}

	public List<Peak> getRange(){
		return this.reversePPM();
	}

	public int getID(){
		return this.timeframeID;
	}

	public double getHighestPPM() {
		return this.highestPPM;
	}

	public double getLowestPPM() {
		return this.lowestPPM;
	}
}
