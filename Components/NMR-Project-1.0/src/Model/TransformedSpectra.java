package Model;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;

public class TransformedSpectra {

	Spectra oldSpectra;
	ArrayList<CorrelationRange> ranges;
	ArrayList<CorrelationPeak> allPeaks;

	public TransformedSpectra(Spectra oldSpectra){
		this.oldSpectra = oldSpectra;
		this.allPeaks = new ArrayList<CorrelationPeak>();
		this.ranges = new ArrayList<CorrelationRange>();
		this.populatePeaks();
		this.transformSpectrum();
	}

	private void populatePeaks(){
		for(PeakRange pr : this.oldSpectra.getRanges()){
			for(Peak p : pr.getRange()){
				CorrelationPeak cp = new CorrelationPeak(pr.getID(), p.getY(), p.getX());
				this.allPeaks.add(cp);
			}
		}
	}

	private void transformSpectrum(){
		for(int i=0; i<=10; i++){
			int spectrumID = 0;
			CorrelationRange cr = new CorrelationRange(i);
			for(CorrelationPeak cp : this.allPeaks){
				if((int)cp.getPPMRef() == i){
					cr.addPeak(cp);
					spectrumID = cp.getSpectrumID();
				}
			}

			if(cr.getPeaks().size() < 64){
				for(int j=(cr.getPeaks().size()); j<64; j++){
					cr.addPeak(new CorrelationPeak(spectrumID, 0, 0));
				}
			}
			this.ranges.add(cr);
		}
	}

	private void displaySpectra(){
		for(CorrelationRange cr : this.ranges){
			for(CorrelationPeak cp : cr.getPeaks()){
				System.out.println("For PPM " + cr.getPPMRange() + " : " + cp.getIntensity() + ", PPM Reference: " + cp.getPPMRef());
			}
			System.out.println("Size of transformed set: " + cr.getPeaks().size());
		}
	}

	public static double round(double value, int places) {
		if (places < 0) throw new IllegalArgumentException();

		BigDecimal bd = new BigDecimal(value);
		bd = bd.setScale(places, RoundingMode.HALF_UP);
		return bd.doubleValue();
	}
	
	public ArrayList<CorrelationRange> getRanges(){
		return this.ranges;
	}
}
