package Model.CorrelationEngine;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Observable;
import java.util.Set;
import java.util.TreeSet;

import Model.CorrelationPeak;
import Model.CorrelationRange;
import Model.Peak;
import Model.PeakRange;
import Model.Quantiser;
import Model.Spectra;
import Model.TransformedSpectra;
import Models.PearsonsCalculator.PearsonsCalculator;
import Models.PearsonsCalculator.PearsonsCoefficient;

public class CorrelationEngineTwo extends Observable{

	Spectra data;
	Spectra processedData;
	CorrelationMatrix CM;
	List<PearsonsCoefficient> rankings; 
	double euclidianDistance;
	Set<Integer> ppmRange;
	double processRange;
	TransformedSpectra transformedSpec;
	Quantiser quantiser;

	public CorrelationEngineTwo(Spectra data, Quantiser quantiser){
		this.data = data;
		CM = new CorrelationMatrix();
		this.rankings = new ArrayList<PearsonsCoefficient>();
		this.quantiser = quantiser;

		Spectra newSpectra = formHighestPeaksPerSpectrum(processSpectra());

		transformedSpec = new TransformedSpectra(newSpectra);
	}

	public TransformedSpectra getTSpectra(){
		return this.transformedSpec;
	}

	private Spectra processSpectra(){
		this.processedData = new Spectra(new ArrayList<PeakRange>());
		for(PeakRange pr : this.data.sortedRange()){
			this.processedData.addRange(pr);
		}

		return this.processedData;
	}

	private Spectra formHighestPeaksPerSpectrum(Spectra orderedSpectra){
		this.processedData = new Spectra(new ArrayList<PeakRange>());

		if(quantiser.getPlaces() == 0){
			for(PeakRange pr : orderedSpectra.getRanges()){
				PeakRange newRange = new PeakRange(new ArrayList<Peak>(), pr.getID());
				for(int i=10; i>=0; i--){
					ArrayList<Peak> peaks = pr.getPeaksAt(i);
					double max_intensity = Double.MIN_VALUE;
					double ppm_for_max = 0.0;
					double specID_for_max = 0.0;
					for(Peak p : peaks){
						if(p.getY() >= max_intensity){
							max_intensity = p.getY();
							ppm_for_max = p.getX();
							specID_for_max = p.getZ();
						}
					}
					newRange.addPeak(new Peak(ppm_for_max, max_intensity, specID_for_max));
				}
				this.processedData.addRange(newRange);
			}
		}else if(quantiser.getPlaces() == 1){
			for(PeakRange pr : orderedSpectra.getRanges()){
				PeakRange newRange = new PeakRange(new ArrayList<Peak>(), pr.getID());
				for(int i=0; i<=10; i++){
					for(int j=0; j<=9; j++){
						double d = Double.parseDouble(i + "." + j);
						ArrayList<Peak> peaks = pr.getPeaksAt(d, 1);
						if(peaks.isEmpty()){
							newRange.addPeak(new Peak(d, 0.0, pr.getID()));
						}else{
							double max_intensity = Double.MIN_VALUE;
							double ppm_for_max = 0.0;
							double specID_for_max = 0.0;
							for(Peak p : peaks){
								if(p.getY() >= max_intensity){
									max_intensity = p.getY();
									ppm_for_max = p.getX();
									specID_for_max = p.getZ();
								}
							}
							newRange.addPeak(new Peak(ppm_for_max, max_intensity, specID_for_max));
						}
					}	
				}
			}
		}
		return this.processedData;
	}

	public void calculatePearsonsCoefficient(){
		boolean duplicates = false;
		for(CorrelationRange cr : this.transformedSpec.getRanges()){
			for(CorrelationRange crg : this.transformedSpec.getRanges()){
				if(!cr.equals(crg)){
					for(PearsonsCoefficient pc : rankings){
						if((pc.getFirstVector().equals(crg) || pc.getFirstVector().equals(cr))
								&& (pc.getSecondVector().equals(crg) || pc.getSecondVector().equals(cr))){
							duplicates = true;
						}
					}
					if(!duplicates){
						PearsonsCalculator pearsons = new PearsonsCalculator(cr.getPeaks(), crg.getPeaks());
						double pearsonsR = pearsons.calculateR();
						if(pearsonsR != 0.0){
							rankings.add(new PearsonsCoefficient(cr, crg, pearsonsR));
							Collections.sort(rankings, PearsonsCoefficient.PearsonsCoefficientComparator);
						}
					}
					duplicates = false;
				}
			}
		}

		for(PearsonsCoefficient pc : rankings){
			//			System.out.println("Pearsons Coefficient is " + pc.getEuclidianDistance() + " for " + pc.getFirstVector().getPPMRange() + " ::: " + pc.getSecondVector().getPPMRange());
		}

		this.setChanged();
		this.notifyObservers();
	}

	public List<PearsonsCoefficient> getRankings(){
		return this.rankings;
	}

	public void notifyQuantiser(Quantiser quantiser2) {
		CM = new CorrelationMatrix();
		this.rankings = new ArrayList<PearsonsCoefficient>();
		this.quantiser = quantiser2;

		Spectra newSpectra = formHighestPeaksPerSpectrum(processSpectra());

		transformedSpec = new TransformedSpectra(newSpectra);
		
	}
}