//package Model.CorrelationEngine;
//
//import java.math.BigDecimal;
//import java.math.RoundingMode;
//import java.util.ArrayList;
//import java.util.List;
//import java.util.Observable;
//import java.util.Set;
//import java.util.TreeSet;
//import java.util.concurrent.ExecutorService;
//import java.util.concurrent.Executors;
//
//import Model.Peak;
//import Model.PeakRange;
//import Model.Spectra;
//import Models.PearsonsCalculator.PearsonsCalculator;
//import Models.PearsonsCalculator.PearsonsCoefficient;
//
//public class CorrelationEngine extends Observable{
//
//	Spectra data;
//	Spectra processedData;
//	CorrelationMatrix CM;
//	List<PearsonsCoefficient> rankings; 
//	double euclidianDistance;
//	Set<Double> ppmRange;
//	double processRange;
//
//	public CorrelationEngine(Spectra data){
//		long startTime = System.currentTimeMillis();
//		this.data = data;
//		CM = new CorrelationMatrix();
//		rankings = new ArrayList<PearsonsCoefficient>();
//		this.euclidianDistance = 0.0;
//		processRange = 0.0;
//		processedData = new Spectra(new ArrayList<PeakRange>());
//
//		double maxPPM = Double.MIN_VALUE;
//
//		/**
//		 * For each peak range
//		 * 		Create a new range
//		 * 		For each whole number in the PPM range 0 - 10
//		 * 			Calculate the highest intensity value and store it as a peak
//		 * 			
//		 * 
//		 */
//		for(PeakRange pr : data.sortedRange()){
//			PeakRange newRange = new PeakRange(new ArrayList<Peak>(), pr.getID());
//			for(int i=0; i<=10; i++){
//				double maxIntens = Double.MIN_VALUE;
//				double correspondingPPM = 0.0;
//				double correspondingSpectrum = 0.0;
//				for(Peak p : pr.getRange()){
//					if(((int) p.getX()) == i){
//						if(p.getY() >= maxIntens){
//							maxIntens = p.getY();
//							correspondingPPM = p.getX();
//							correspondingSpectrum = p.getZ();
//						}
//					}
//				}
//				if(maxIntens != Double.MIN_VALUE){
//					newRange.addPeak(new Peak(correspondingPPM, maxIntens, correspondingSpectrum));
//				}
//			}
//			processedData.addRange(newRange);
//		}
//
//
//		this.populateMatrix();
//
//		for(Double d : ppmRange){
//			if(d > maxPPM){
//				maxPPM = d;
//			}
//		}
//
//		long endTime = System.currentTimeMillis();
//		System.out.println("Correlations Execution Time: " + (endTime - startTime));
//	}
//
//	public static double round(double value, int places) {
//		if (places < 0) throw new IllegalArgumentException();
//
//		BigDecimal bd = new BigDecimal(value);
//		bd = bd.setScale(places, RoundingMode.HALF_UP);
//		return bd.doubleValue();
//	}
//
//	public void setProcessRange(Double range){
//		this.processRange = range;
//	}
//
//	private void populateMatrix(){
//		ppmRange = new TreeSet<Double>();
//		for(PeakRange p : processedData.sortedRange()){
//			for(Peak peak : p.getRange()){
//				ppmRange.add(peak.getX());
//			}
//		}
//
//		ArrayList<Double> intensities = new ArrayList<Double>();
//		for(Double d : ppmRange){
//			for(PeakRange p : processedData.sortedRange()){
//				for(Peak peak : p.getRange()){
//					if(peak.getX() >= d && peak.getX() <= d+1){
//						intensities.add(peak.getX());
//					}
//				}
//			}
//			CM.addMapping(d, intensities);
//			intensities = new ArrayList<Double>();
//		}
//	}
//
//	public void calculatePearsonsCoefficient(){
//		for(Double d1 : ppmRange){
//			ArrayList<Double> primaryVector = CM.getValues(d1);
//			for(Double d2 : ppmRange){
//				if(d1 != d2){
//					ArrayList<Double> secondaryVector = CM.getValues(d2);
//					if(primaryVector.size() == secondaryVector.size()){
//						PearsonsCalculator pearsons = new PearsonsCalculator(primaryVector, secondaryVector);
//						double pearsonsR = pearsons.calculateR();
//						rankings.add(new PearsonsCoefficient(primaryVector, secondaryVector, pearsonsR));
//						System.out.println("Pearsons Coefficient is " + pearsonsR + " for " + primaryVector + " ::: " + secondaryVector);
//					}
//				}
//			}
//		}
//		System.out.println("Finished Calculating Correlations");
//		this.setChanged();
//		this.notifyObservers();
//	}
//
//	public List<PearsonsCoefficient> getRankings(){
//		return this.rankings;
//	}
//}