package Model.CorrelationEngine;

import Model.CorrelationRange;
import Model.CorrelationPeak;
import Model.TransformedSpectra;

public class BackPropagator {
	
	TransformedSpectra spectra;
	
	public BackPropagator(TransformedSpectra spectra){
		this.spectra = spectra;
		this.begin();
	}
	
	private void begin(){
		for(CorrelationRange cr : this.spectra.getRanges()){
			System.out.println("PEAK RANGE " + cr.getPPMRange());
			for(CorrelationPeak cp : cr.getPeaks()){
				System.out.println("Spectrum ID: " + cp.getSpectrumID() + ", PPM REF: " + cp.getPPMRef() + ", Intensity: " + cp.getIntensity());
			}
		}
	}
	
//	private double nearestLowVal(){
//		
//	}
//	
//	private double nearestHighVal(){
//		
//	}
}
