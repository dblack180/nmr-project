package Model.CorrelationEngine;

import javax.swing.JPanel;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYAreaRenderer;

import Model.CorrelationPeak;
import Model.CorrelationRange;
import Model.Peak;
import Model.PeakRange;
import Model.Spectra;

public class CorrelationGraph {

	Spectra NMRSpectra;
	CorrelationRange cr1;
	CorrelationRange cr2;
	double lowerRegion = 0.0;
	double higherRegion = 0.0;

	public CorrelationGraph(Spectra spectra, CorrelationRange cr1, CorrelationRange cr2){
		this.setSpectra(spectra);
		this.cr1 = cr1;
		this.cr2 = cr2;
	}

	public CorrelationGraph(Spectra spectra, CorrelationRange cr1,
			CorrelationRange cr2, double lowerRegion,
			double higherRegion) {
		this.setSpectra(spectra);
		this.cr1 = cr1;
		this.cr2 = cr2;
		this.lowerRegion = lowerRegion;
		this.higherRegion = higherRegion;
		System.out.println("HIGHER REGION " + this.higherRegion);
		System.out.println("LOWER REGION " + this.lowerRegion);
	}

	private void setSpectra(Spectra newSpectra){
		this.NMRSpectra = newSpectra;
	}

	public Spectra getSpectra(){
		return this.NMRSpectra;
	}

	private XYDataset createNMRDataset(){

		if(this.lowerRegion != 0.0 && this.higherRegion != 0.0){
			double i = 0.0;
			final XYSeriesCollection dataset = new XYSeriesCollection();
			final XYSeries corrSeriesA = new XYSeries("Correlation Vect 1");
			final XYSeries corrSeriesB = new XYSeries("Correlation Vect 2");
			for(PeakRange r : getSpectra().sortedRange()){
				final XYSeries series = new XYSeries("Spectrum " +  r.getID());
				for(Peak p : r.getRange()){
					if(p.getX() >= this.lowerRegion && p.getX() <= this.higherRegion){
						boolean corrPlotted = false;
						for(CorrelationPeak cp : this.cr1.getPeaks()){
							if(cp.getPPMRef() >= this.lowerRegion && cp.getPPMRef() <= this.higherRegion){
								if(p.getX() == cp.getPPMRef()){
									series.add((double) p.getX() + i,(double) p.getY() + i);
									corrSeriesA.add((double) (p.getX() + i), (double) p.getY()+2);
									corrPlotted = true;
								}
							}
						}
						for(CorrelationPeak cp : this.cr2.getPeaks()){	
							if(cp.getPPMRef() >= this.lowerRegion && cp.getPPMRef() <= this.higherRegion){
								if(p.getX() == cp.getPPMRef()){
									series.add((double) p.getX() + i,(double) p.getY() + i);
									corrSeriesB.add((double) (p.getX() + i), (double) p.getY()+2);
									corrPlotted = true;
								}
							}
						}
						if(!corrPlotted){
							if(p.getX() >= this.lowerRegion && p.getX() <= this.higherRegion){
								series.add((double) p.getX() + i,(double) p.getY() + i);
							}
						}
					}
				}
				i+=0.01;
				dataset.addSeries(series);
			}

			dataset.addSeries(corrSeriesA);
			dataset.addSeries(corrSeriesB);
			return dataset;
		}else{
			double i = 0.0;
			final XYSeriesCollection dataset = new XYSeriesCollection();
			final XYSeries corrSeriesA = new XYSeries("Correlation Vect 1");
			final XYSeries corrSeriesB = new XYSeries("Correlation Vect 2");
			for(PeakRange r : getSpectra().sortedRange()){
				final XYSeries series = new XYSeries("Spectrum " +  r.getID());
				for(Peak p : r.getRange()){
					boolean corrPlotted = false;
					for(CorrelationPeak cp : this.cr1.getPeaks()){
						if(p.getX() == cp.getPPMRef()){
							series.add((double) p.getX() + i,(double) p.getY() + i);
							corrSeriesA.add((double) (p.getX() + i), (double) p.getY()+2);
							corrPlotted = true;
						}
					}
					for(CorrelationPeak cp : this.cr2.getPeaks()){	
						if(p.getX() == cp.getPPMRef()){
							series.add((double) p.getX() + i,(double) p.getY() + i);
							corrSeriesB.add((double) (p.getX() + i), (double) p.getY()+2);
							corrPlotted = true;
						}
					}
					if(!corrPlotted){
						series.add((double) p.getX() + i,(double) p.getY() + i);
					}
				}
				i+=0.01;
				dataset.addSeries(series);
			}

			dataset.addSeries(corrSeriesA);
			dataset.addSeries(corrSeriesB);
			return dataset;
		}
	}

	public ChartPanel getCorrelationGraph(JPanel container){
		ChartPanel chart = new ChartPanel(this.createGraph());
		chart.setSize(container.getWidth(), container.getHeight());
		chart.setVisible(true);
		return chart;
	}

	private JFreeChart createGraph(){

		XYDataset dataset = this.createNMRDataset();
		final JFreeChart chart = ChartFactory.createXYLineChart(             
				"Rattlesnake YGG", 
				"PPM",              
				"Intensity",              
				dataset);

		XYPlot plot = (XYPlot) chart.getXYPlot();
		plot.getDomainAxis().setInverted(true);

		plot.setRenderer(new XYAreaRenderer());
		return chart;
	}

}
