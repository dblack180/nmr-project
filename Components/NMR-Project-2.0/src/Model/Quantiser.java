package Model;

public class Quantiser {

	private int places;
	
	public Quantiser(){
		this.places = 0;
	}
	
	public Quantiser(int places){
		if(places >= 4){
			this.places = 4;
		}else if(places <= 0){
			this.places = 0;
		}else{
			this.places = places;	
		}
	}
	
	public int getPlaces(){
		return this.places;
	}
	
	public void increasePlaces(){
		if(this.places == 4){
			this.places = 4;
		}else{
			this.places++;
		}
	}
	
	public void decreasePlaces(){
		if(this.places == 0){
			this.places = 0;
		}else{
			this.places--;
		}
	}
	
	public void setPlaces(int newPlaces){
		this.places = newPlaces;
	}
}
