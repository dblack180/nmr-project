package Model;

import java.util.ArrayList;

public class CorrelationRange {

	ArrayList<CorrelationPeak> peaks;
	int ppmRange;
	
	public CorrelationRange(int ppmRange){
		peaks = new ArrayList<CorrelationPeak>();
		this.ppmRange = ppmRange;
	}
	
	public CorrelationRange(int ppmRange, ArrayList<CorrelationPeak> peaks){
		this.peaks = peaks;
		this.ppmRange = ppmRange;
	}
	
	public ArrayList<CorrelationPeak> getPeaks(){
		return this.peaks;
	}
	
	public int getPPMRange(){
		return this.ppmRange;
	}
	
	public void addPeak(CorrelationPeak cp){
		this.peaks.add(cp);
	}
}
