package ModelTests;

import static org.junit.Assert.*;
import static org.easymock.EasyMock.*;

import java.util.ArrayList;
import java.util.List;

import org.easymock.IMockBuilder;
import org.junit.Test;

import Model.Peak;
import Model.PeakRange;
import Model.Spectra;


/**
 * Collaborating Classes: 
 * 
 * PeakRange 
 * Peak via proxy of PeakRange
 * **/

public class SpectraTests {

	Spectra spectra;
	PeakRange mockPR;
	Peak mockPeak;
	IMockBuilder<Peak> peakMockBuilder;
	IMockBuilder<PeakRange> peakRangeMockBuilder;
	
	public void initFixtures(){
		spectra = new Spectra(new ArrayList<PeakRange>());
		peakMockBuilder = createMockBuilder(Peak.class)
				.withConstructor(0.0, 100.0, 10.0);
		peakRangeMockBuilder = createMockBuilder(PeakRange.class)
				.withConstructor(new ArrayList<Peak>(), 0);
		mockPR = peakRangeMockBuilder.createMock();
		mockPeak = peakMockBuilder.createMock();
		mockPR.addPeak(mockPeak);
	}

	@Test
	public void addRangeTest(){
		initFixtures();
		spectra.addRange(mockPR);
		assertTrue(spectra.getRanges().contains(mockPR));
		assertNotEquals(spectra.getRanges().size(), 0);
	}

	@Test
	public void removeRangeTest(){
		initFixtures();
		spectra.addRange(mockPR);
		assertFalse(spectra.getRanges().isEmpty());
		spectra.removeRange(0);
		assertTrue(spectra.getRanges().isEmpty());
	}

	@Test
	public void getRangesTest(){
		initFixtures();
		PeakRange mockPR2 = peakRangeMockBuilder.createMock();
		spectra.addRange(mockPR);
		spectra.addRange(mockPR2);
		
		assertTrue(spectra.getRanges().contains(mockPR));
		assertTrue(spectra.getRanges().contains(mockPR2));
		
		assertEquals(spectra.getRanges().size(), 2);
	}

	@Test
	public void sortedRangeTest(){
		initFixtures();
		peakRangeMockBuilder = createMockBuilder(PeakRange.class)
				.withConstructor(new ArrayList<PeakRange>(), 0);
		spectra.addRange(peakRangeMockBuilder.createMock());
		
		peakRangeMockBuilder = createMockBuilder(PeakRange.class)
				.withConstructor(new ArrayList<PeakRange>(), 1);
		spectra.addRange(peakRangeMockBuilder.createMock());
		
		List<PeakRange> sortedRanges = spectra.sortedRange();
		
		assertTrue(sortedRanges.get(0).getID() == 0);
		assertTrue(sortedRanges.get(1).getID() == 1);
		
	}
}
