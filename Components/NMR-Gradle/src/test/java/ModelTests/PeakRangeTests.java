package ModelTests;

import static org.junit.Assert.*;
import static org.easymock.EasyMock.*;

import java.util.ArrayList;

import org.easymock.IMockBuilder;
import org.junit.Test;

import Model.Peak;
import Model.PeakRange;

public class PeakRangeTests {

	PeakRange peakRange; 
	IMockBuilder<Peak> mockPeakBuilder;
	IMockBuilder<Peak> mockPeakBuilderA;
	IMockBuilder<Peak> mockPeakBuilderB;
	
	public void initFixtures(){
		peakRange = new PeakRange(new ArrayList<Peak>(), 0);
		mockPeakBuilder = createMockBuilder(Peak.class)
				.withConstructor(0.0, 10.0, 0.0);
		mockPeakBuilderA = createMockBuilder(Peak.class)
				.withConstructor(0.0, 20.0, 0.0);
		mockPeakBuilderB = createMockBuilder(Peak.class)
				.withConstructor(0.0, 30.0, 0.0);
	}

	@Test
	public void addPeakNormalTest(){
		initFixtures();
		Peak mockPeak = mockPeakBuilder.createMock();

		peakRange.addPeak(mockPeak);
		assertTrue(peakRange.getRange().contains(mockPeak));
	}

	@Test(expected = IllegalArgumentException.class)
	public void addPeakRegressionTest(){
		initFixtures();
		IMockBuilder<Peak> DuffMockPeakBuilder = createMockBuilder(Peak.class)
				.withConstructor(0.0, 4.9E-324, 0.0);
		Peak mockPeak = DuffMockPeakBuilder.createMock(); 


		peakRange.addPeak(mockPeak);
	}

	@Test
	public void removePeakWithIndexTest(){
		initFixtures();
		Peak mockPeak = mockPeakBuilder.createMock();
		peakRange.addPeak(mockPeak);
		peakRange.removePeak(0);
		assertEquals(peakRange.getRange().isEmpty(), true);
	}

	@Test
	public void getPeakAtTest(){
		initFixtures();
		Peak mockPeakA = mockPeakBuilderA.createMock();
		Peak mockPeakB = mockPeakBuilderB.createMock();
		
		peakRange.addPeak(mockPeakA);
		peakRange.addPeak(mockPeakB);
		
		Peak testPeak = peakRange.getPeakAt(0);
		assertTrue(mockPeakA.equals(testPeak));
	}

	@Test
	public void getPeaksAtTest(){
		initFixtures();
		Peak mockPeakA = mockPeakBuilderA.createMock();
		Peak mockPeakB = mockPeakBuilderB.createMock();
		
		peakRange.addPeak(mockPeakA);
		peakRange.addPeak(mockPeakB);
		
		assertTrue(peakRange.getPeaksAt(0).contains(mockPeakA) && peakRange.getPeaksAt(0).contains(mockPeakB));
	}


}
