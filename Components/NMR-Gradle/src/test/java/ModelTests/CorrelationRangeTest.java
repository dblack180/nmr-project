package ModelTests;

import static org.junit.Assert.*;

import org.junit.Test;

import Model.CorrelationPeak;
import Model.CorrelationRange;

public class CorrelationRangeTest {

	CorrelationRange correlationRange;
	
	public void initFixtures(){
		correlationRange = new CorrelationRange(0);
	}
	
	@Test
	public void getPPMLowTest(){
		initFixtures();
		assertTrue(correlationRange.getPPMLow() == 0);
		System.out.println("Correlation Range - Retrieve PPM Low unit test - Complete");
	}
	
	@Test
	public void containsSpectraRecordTest(){
		initFixtures();
		correlationRange.addPeak(new CorrelationPeak(0, 15.0, 12.0));
		assertTrue(correlationRange.containsSpectraRecord(0));
		System.out.println("Correlation Range - Contains spectra record unit test - Complete");
	}
	
	@Test
	public void addPeakTest(){
		initFixtures();
		correlationRange.addPeak(new CorrelationPeak(0, 15.0, 12.0));
		assertFalse(correlationRange.getPeaks().isEmpty());
		System.out.println("Correlation Range - Add Peak unit test - Complete");
	}
	
	@Test
	public void removePeakTest(){
		initFixtures();
		CorrelationPeak cp = new CorrelationPeak(0, 15.0, 12.0);
		correlationRange.addPeak(cp);
		assertFalse(correlationRange.getPeaks().isEmpty());
		correlationRange.removePeak(cp);
		assertTrue(correlationRange.getPeaks().isEmpty());
		System.out.println("Correlation Range - Remove Peak unit test - Complete");
	}
	
	@Test
	public void containsZeroTest(){
		initFixtures();
		CorrelationPeak cp = new CorrelationPeak(0, 15.0, 0.75);
		correlationRange.addPeak(cp);
		assertTrue(correlationRange.containsZeros());
		correlationRange.removePeak(cp);
		cp = new CorrelationPeak(0, 15.0, 1.1);
		assertFalse(correlationRange.containsZeros());
		System.out.println("Correlation Range - Range Contains Zero PPM Peaks unit test - Complete");
	}
}
