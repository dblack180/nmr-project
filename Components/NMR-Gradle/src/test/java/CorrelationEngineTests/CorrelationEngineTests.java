package CorrelationEngineTests;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.easymock.IMockBuilder;
import org.junit.Test;

import static org.easymock.EasyMock.*;
import CorrelationEngine.CorrelationEngine;
import Model.Peak;
import Model.PeakRange;
import Model.Quantiser;
import Model.Spectra;

public class CorrelationEngineTests {
	
	/**
	 * Object Fixtures to be mocked 
	 * **/
	CorrelationEngine CE;
	Spectra spectra;
	Quantiser quantiser;
	
	/**
	 * Mock Builders for objects
	 * **/
	IMockBuilder<CorrelationEngine> CEMockBuilder;
	IMockBuilder<Spectra> spectraMockBuilder;
	IMockBuilder<Quantiser> quantiserMockBuilder;
	IMockBuilder<PeakRange> peakRangeMockBuilder;
	IMockBuilder<Peak> peakMockBuilder;

	
	public void initFixtures(){
		spectraMockBuilder = createMockBuilder(Spectra.class)
				.withConstructor(new ArrayList<PeakRange>());
		spectra = spectraMockBuilder.createMock();
		
		quantiserMockBuilder = createMockBuilder(Quantiser.class)
				.withConstructor(0);
		quantiser = quantiserMockBuilder.createMock();
		
		CE = new CorrelationEngine(spectra, quantiser);
		
	}
	
	public void initFixturesTestData(){
		spectraMockBuilder = createMockBuilder(Spectra.class)
				.withConstructor(new ArrayList<PeakRange>());
		spectra = spectraMockBuilder.createMock();
		
		peakRangeMockBuilder = createMockBuilder(PeakRange.class)
				.withConstructor(new ArrayList<Peak>(), 0);
		peakMockBuilder = createMockBuilder(Peak.class)
				.withConstructor(1.0, 50.0, 1.0);
		
		PeakRange prA = peakRangeMockBuilder.createMock();
		prA.addPeak(peakMockBuilder.createMock());
		peakMockBuilder = createMockBuilder(Peak.class)
				.withConstructor(1.1, 50.0, 1.0);
		prA.addPeak(peakMockBuilder.createMock());
		
		spectra.addRange(prA);
		
		peakRangeMockBuilder = createMockBuilder(PeakRange.class)
				.withConstructor(new ArrayList<Peak>(), 0);
		peakMockBuilder = createMockBuilder(Peak.class)
				.withConstructor(1.0, 50.0, 2.0);
		
		PeakRange prB = peakRangeMockBuilder.createMock();
		prB.addPeak(peakMockBuilder.createMock());
		peakMockBuilder = createMockBuilder(Peak.class)
				.withConstructor(1.1, 50.0, 2.0);
		prB.addPeak(peakMockBuilder.createMock());
		
		spectra.addRange(prB);
		
		quantiserMockBuilder = createMockBuilder(Quantiser.class)
				.withConstructor(0);
		quantiser = quantiserMockBuilder.createMock();
		
		CE = new CorrelationEngine(spectra, quantiser);	
	}
	
	@Test(expected = IndexOutOfBoundsException.class)
	public void calculatePearsonsCoefficientRegressionTest(){
		initFixtures();
		CE.calculatePearsonsCoefficient();
		System.out.println("Correlation Engine - Calculate Pearsons Coefficient Regression Test - Complete");
	}
	
	@Test
	public void calculatePearsonsCoefficientTest(){
		initFixturesTestData();
		CE.calculatePearsonsCoefficient();
		assertFalse(CE.getRankings().isEmpty());
		assertEquals(CE.getRankings().size(), 1);
		System.out.println("Correlation Engine - Calculate Pearsons Coefficient Unit Test - Complete");
	}
}
