import org.junit.runner.RunWith;
import org.junit.runners.Suite;

import ModelTests.CorrelationRangeTest;
import ModelTests.PeakBucketTests;
import ModelTests.PeakBucketTests;
import ModelTests.PeakRangeTests;
import ModelTests.SpectraTests;
import ModelTests.SpectraTests;
import PearsonsCalculatorTests.PearsonsCalculatorTests;
import UtilsTests.PeakFileReaderTests;
import CorrelationEngineTests.CorrelationEngineTests;
@RunWith(Suite.class)
@Suite.SuiteClasses({
   CorrelationRangeTest.class,
   PeakBucketTests.class,
   PeakRangeTests.class,
   SpectraTests.class,
   PearsonsCalculatorTests.class,
   PeakFileReaderTests.class,
   CorrelationEngineTests.class
})
public class NMRTestSuite {   
} 