package PearsonsCalculatorTests;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Test;

import static org.easymock.EasyMock.*;
import Model.CorrelationPeak;
import PearsonsCalculator.PearsonsCalculator;

public class PearsonsCalculatorTests {

	ArrayList<CorrelationPeak> v1;
	ArrayList<CorrelationPeak> v2;
	PearsonsCalculator pearsons;

	public void initFixtures(){
		v1 = new ArrayList<CorrelationPeak>();
		v2 = new ArrayList<CorrelationPeak>();
		CorrelationPeak cp = new CorrelationPeak(0, 1, 1);
		v1.add(cp);
		cp = new CorrelationPeak(1, 2, 1);
		v1.add(cp);
		cp = new CorrelationPeak(2, 3, 1);
		v1.add(cp);
		cp = new CorrelationPeak(3, 4, 1);
		v1.add(cp);
		cp = new CorrelationPeak(4, 5, 1);
		v1.add(cp);
		cp = new CorrelationPeak(0, 10, 1);
		v2.add(cp);
		cp = new CorrelationPeak(1, 20, 1);
		v2.add(cp);
		cp = new CorrelationPeak(2, 30, 1);
		v2.add(cp);
		cp = new CorrelationPeak(3, 40, 1);
		v2.add(cp);
		cp = new CorrelationPeak(4, 50, 1);
		v2.add(cp);
	
		
		pearsons = new PearsonsCalculator(v1, v2);
	}
	
	public void initFixturesUneven(){
		v1 = new ArrayList<CorrelationPeak>();
		v2 = new ArrayList<CorrelationPeak>();
		
		for(int i=0; i<5; i++){
			v1.add(createMock(CorrelationPeak.class));
			v2.add(createMock(CorrelationPeak.class));
		}
		v2.add(createMock(CorrelationPeak.class));
		
		pearsons = new PearsonsCalculator(v1, v2);
	}
	
	@Test
	public void calculateRTest(){
		initFixtures();
		assertTrue(pearsons.calculateR() == 1);
	}
	
	@Test(expected = IllegalArgumentException.class) 
	public void catchIllegalArgumentTest(){
		initFixturesUneven();
	}
}
