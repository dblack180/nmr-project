package CorrelationEngine;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Observable;
import java.util.Set;

import Model.BucketContainer;
import Model.CorrelationRange;
import Model.Peak;
import Model.PeakBucket;
import Model.PeakRange;
import Model.Quantiser;
import Model.Spectra;
import Model.TransformedSpectra;
import PearsonsCalculator.PearsonsCalculator;
import PearsonsCalculator.PearsonsCoefficient;

public class CorrelationEngine extends Observable{

	Spectra data;
	Spectra processedData;
	CorrelationMatrix CM;
	List<PearsonsCoefficient> rankings; 
	double euclidianDistance;
	Set<Integer> ppmRange;
	double processRange;
	TransformedSpectra transformedSpec;
	Quantiser quantiser;
	double MIN_DISTANCE = 0.0;
	ArrayList<BucketContainer> bContainers;

	public CorrelationEngine(Spectra data, Quantiser quantiser){
		this.data = data;
		this.rankings = new ArrayList<PearsonsCoefficient>();
		this.quantiser = quantiser;

		Spectra newSpectra = formHighestPeaksPerSpectrum(processSpectra());

		transformedSpec = new TransformedSpectra(this.bContainers, this.MIN_DISTANCE);
	}

	public TransformedSpectra getTSpectra(){
		return this.transformedSpec;
	}

	private Spectra processSpectra(){
		this.processedData = new Spectra(new ArrayList<PeakRange>());
		for(PeakRange pr : this.data.sortedRange()){
			this.processedData.addRange(pr);
		}

		return this.processedData;
	}

	private boolean isPeak(PeakRange range, Peak peak){

		Peak prevPeak = null;
		Peak currPeak = null;
		Peak nextPeak = null;
		Object[] primitivePeaks = range.getRange().toArray();
		ArrayList<Peak> peaks = new ArrayList<Peak>();
		peaks.addAll(range.getRange());
		int i = 0;

		//		Initial catch if intensity is too low
		if(peak.getY() <= 1.0){
			return false;
		}

		try{
			for(Object p : primitivePeaks){
				Peak cp = (Peak) p;
				if(p.equals(peak)){
					Peak pp = (Peak) primitivePeaks[i-1];
					prevPeak = pp;
					currPeak = cp;
					Peak np = (Peak) primitivePeaks[i+1];
					nextPeak = np;
				}
				i++;
			}
		}catch(Exception e){
			return false;
		}

		if((currPeak.getY() > prevPeak.getY()) && (currPeak.getY() > nextPeak.getY())){
			return true;
		}

		return false;
	}

	private void setMinDistance(ArrayList<BucketContainer> bc){
		this.MIN_DISTANCE = 0.05;
	}
	
	private Spectra formHighestPeaksPerSpectrum(Spectra orderedSpectra){
		long startTime = System.currentTimeMillis();
		/**
		 * 1.1 For Peak Range 0
		 * 1.2 		Identify peaks within each ppm range
		 * 1.3.1 		Store each peak into Peak Bucket identified by Spectra ID and PPM range
		 * 1.3.2		PPM, Intensity and Spectra ID
		 * 1.3.3		Store order of peak in ppm range
		 * 2.1 Store all spectra 0 Buckets into bucket container and keep as reference
		 * 2.2 Define minimum distance between peaks
		 * 3.1 Follow Steps 1.1 - 1.3.3 using first Peak Bucket as reference to find new peaks allocating ± minimum distance as padding
		 * 4.1 For all bucket containers:
		 * 4.2 		For all buckets:
		 * 4.3 			Store ordered peaks into new range
		 * 4.4		Store new range into spectra 
		 * 5.1 Return new spectra
		 ********** NEED TO MODIFY ALGORITHM *********** 
		 * **/

		bContainers = new ArrayList<BucketContainer>();
		for(PeakRange pr : orderedSpectra.getRanges()){
			BucketContainer bucketCont = new BucketContainer(pr.getID());
			for(int i=12; i>0; i--){
				ArrayList<Peak> peaks = pr.getPeaksAt(i);
				PeakBucket bucket = new PeakBucket(pr.getID(), i);
				int order = 0;
				for(Peak peak: peaks){
					if(isPeak(pr, peak)){			
						bucket.addPeak(order, peak);
						order++;
					}
				}
				bucketCont.addBucket(bucket);
			}
			bContainers.add(bucketCont);
		}
		
		this.setMinDistance(bContainers);
		
		this.processedData = new Spectra(new ArrayList<PeakRange>());
		
		for(BucketContainer bc : bContainers){
			PeakRange newRange = new PeakRange(new ArrayList<Peak>(), bc.getSpectraID());
			for(PeakBucket pb : bc.getBuckets()){
				for(Peak p : pb.getAllPeaks()){
					try {
						newRange.addPeak(p);
					} catch (IllegalArgumentException e) {
						e.printStackTrace();
					}
				}
			}
			this.processedData.addRange(newRange);
		}
		
		long endTime = System.currentTimeMillis();
		
		System.out.println("Time to finish Peak Allocation: " + (endTime - startTime));
		
		return processedData;
	}

	public void calculatePearsonsCoefficient(){
		long startTime = System.currentTimeMillis();
		boolean duplicates = false;
		for(CorrelationRange cr : this.transformedSpec.getRanges()){
			for(CorrelationRange crg : this.transformedSpec.getRanges()){
				if(!cr.equals(crg)){
					for(PearsonsCoefficient pc : rankings){
						if((pc.getFirstVector().equals(crg) || pc.getFirstVector().equals(cr))
								&& (pc.getSecondVector().equals(crg) || pc.getSecondVector().equals(cr))){
							duplicates = true;
						}
					}
					if(!duplicates){
						PearsonsCalculator pearsons = new PearsonsCalculator(cr.getPeaks(), crg.getPeaks());
						double pearsonsR = pearsons.calculateR();
						if(pearsonsR != 0.0 || (!cr.containsZeros() && !crg.containsZeros())){
							rankings.add(new PearsonsCoefficient(cr, crg, pearsonsR));
							Collections.sort(rankings, PearsonsCoefficient.PearsonsCoefficientComparator);
						}
					}
					duplicates = false;
				}
			}
		}
		long endTime = System.currentTimeMillis();
		System.out.println("Time to finish CE: " + (endTime - startTime));
		
		this.setChanged();
		this.notifyObservers();
	}

	public List<PearsonsCoefficient> getRankings(){
		return this.rankings;
	}

	public void notifyQuantiser(Quantiser quantiser2) {
		this.rankings = new ArrayList<PearsonsCoefficient>();
		this.quantiser = quantiser2;

		Spectra newSpectra = formHighestPeaksPerSpectrum(processSpectra());

		transformedSpec = new TransformedSpectra(this.bContainers, this.MIN_DISTANCE);

	}
}