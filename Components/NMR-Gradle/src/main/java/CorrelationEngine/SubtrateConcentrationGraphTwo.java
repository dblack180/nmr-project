package CorrelationEngine;

import javax.swing.JPanel;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.XYPlot;

import Model.CorrelationPeak;
import Model.CorrelationRange;
import Model.TransformedSpectra;

public class SubtrateConcentrationGraphTwo {

	CorrelationEngine CE;
	TransformedSpectra TS;
	ChartPanel first = null;
	ChartPanel second = null;
	double fstPPM = 0.0;
	double sndPPM = 0.0;
	
	public SubtrateConcentrationGraphTwo(CorrelationEngine CE){
		this.CE = CE;
		this.TS = CE.getTSpectra();
	}

	public void initialiseSubtrateGraphsTwo(JPanel panel1, JPanel panel2){
		first = new ChartPanel(this.createFirstSubtrate());
		first.setSize(panel1.getWidth(), panel1.getHeight());
		first.setVisible(true);
		second = new ChartPanel(this.createSecondSubtrate());
		second.setSize(panel2.getWidth(), panel2.getHeight());
		second.setVisible(true);
	}
	
	public void initialiseSubtrateGraphsTwo(JPanel panel1, JPanel panel2, double firstPpmRange, double firstPpmRangeHigh, double secondPpmRange, double secondPpmRangeHigh){
		first = new ChartPanel(this.createFirstSubtrate(firstPpmRange, firstPpmRangeHigh));
		first.setSize(panel1.getWidth(), panel1.getHeight());
		first.setVisible(true);
		second = new ChartPanel(this.createSecondSubtrate(secondPpmRange, secondPpmRangeHigh));
		second.setSize(panel2.getWidth(), panel2.getHeight());
		second.setVisible(true);
	}
	
	public ChartPanel getFirst(){
		return this.first;
	}
	
	public ChartPanel getSecond(){
		return this.second;
	}
	
	private JFreeChart createFirstSubtrate(){
		XYDataset dataset = this.firstSubtrateDataSet();
		final JFreeChart chart = ChartFactory.createXYLineChart(             
				"PPM Range 0 - 1", 
				"PPM",              
				"Intensity",              
				dataset);

		XYPlot plot = (XYPlot) chart.getXYPlot();

//		plot.setRenderer(new XYAreaRenderer());
		return chart;
	}
	
	private JFreeChart createSecondSubtrate(){
		XYDataset dataset = this.secondSubtrateDataSet();
		final JFreeChart chart = ChartFactory.createXYLineChart(             
				"PPM Range 1 - 2", 
				"PPM",              
				"Intensity",              
				dataset);

		XYPlot plot = (XYPlot) chart.getXYPlot();

//		plot.setRenderer(new XYAreaRenderer());
		return chart;
	}
	
	private JFreeChart createFirstSubtrate(double ppmRange, double ppmRangeHigh){
		XYDataset dataset = this.firstSubtrateDataSet(ppmRange, ppmRangeHigh);
		final JFreeChart chart = ChartFactory.createXYLineChart(             
				"PPM Range " + ppmRange + " - " + ppmRangeHigh, 
				"PPM",              
				"Intensity",              
				dataset);

		XYPlot plot = (XYPlot) chart.getXYPlot();

//		plot.setRenderer(new XYAreaRenderer());
		return chart;
	}
	
	private JFreeChart createSecondSubtrate(double ppmRange, double ppmRangeHigh){
		XYDataset dataset = this.secondSubtrateDataSet(ppmRange, ppmRangeHigh);
		final JFreeChart chart = ChartFactory.createXYLineChart(             
				"PPM Range " + ppmRange + " - " + ppmRangeHigh, 
				"PPM",              
				"Intensity",              
				dataset);

		XYPlot plot = (XYPlot) chart.getXYPlot();

//		plot.setRenderer(new XYAreaRenderer());
		return chart;
	}
	
	private XYDataset firstSubtrateDataSet(){
		CorrelationRange CR0 = this.TS.getRanges().get(0);
		final XYSeriesCollection dataset = new XYSeriesCollection();
		final XYSeries sbstr = new XYSeries(CR0.getPPMRange());
		for(CorrelationPeak cp : CR0.getPeaks()){
			sbstr.add(cp.getSpectrumID(), cp.getIntensity());
		}
		
		dataset.addSeries(sbstr);
		return dataset;
	}
	
	private XYDataset secondSubtrateDataSet(){
		CorrelationRange CR1 = this.TS.getRanges().get(1);
		final XYSeriesCollection dataset = new XYSeriesCollection();
		final XYSeries sbstr = new XYSeries(CR1.getPPMRange());
		for(CorrelationPeak cp : CR1.getPeaks()){
			sbstr.add(cp.getSpectrumID(), cp.getIntensity());
		}
		
		dataset.addSeries(sbstr);
		return dataset;
	}
	
	private XYDataset firstSubtrateDataSet(double ppmRange, double ppmRangeHigh){
		this.fstPPM = ppmRange;
		CorrelationRange CR = this.TS.getPeaksAt(ppmRange, ppmRangeHigh);
		final XYSeriesCollection dataset = new XYSeriesCollection();
		final XYSeries sbstr = new XYSeries(CR.getPPMRange());
		for(CorrelationPeak cp : CR.getPeaks()){
			sbstr.add(cp.getSpectrumID(), cp.getIntensity());
		}
		
		dataset.addSeries(sbstr);
		return dataset;
	}
	
	private XYDataset secondSubtrateDataSet(double ppmRange, double ppmRangeHigh){
		this.sndPPM = ppmRange;
		CorrelationRange CR = this.TS.getPeaksAt(ppmRange, ppmRangeHigh);
		final XYSeriesCollection dataset = new XYSeriesCollection();
		final XYSeries sbstr = new XYSeries(CR.getPPMRange());
		for(CorrelationPeak cp : CR.getPeaks()){
			sbstr.add(cp.getSpectrumID(), cp.getIntensity());
		}
		
		dataset.addSeries(sbstr);
		return dataset;
	}
}
