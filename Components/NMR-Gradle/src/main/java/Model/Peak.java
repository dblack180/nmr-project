package Model;

public class Peak {

	private double ppm;
	private double intensity;
	private double timeframe;
	
	public Peak(double x, double y, double z){
		this.ppm = x;
		this.intensity = y;
		this.timeframe = z;
	}
	
	public double getX(){
		return this.ppm;
	}
	
	public double getY(){
		return this.intensity;
	}
	
	public double getZ(){
		return this.timeframe;
	}
}

