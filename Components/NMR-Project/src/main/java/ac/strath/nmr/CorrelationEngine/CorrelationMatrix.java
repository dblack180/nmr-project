package ac.strath.nmr.CorrelationEngine;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class CorrelationMatrix {

	Map<Integer, ArrayList<Double>> matrix;
	
	public CorrelationMatrix(){
		matrix = new HashMap<Integer, ArrayList<Double>>();
	}
	
	public void addMapping(int ppm, ArrayList<Double> intensities){
		matrix.put(ppm, intensities);
	}
	
	public void removeMapping(int ppm){
		matrix.remove(ppm);
	}
	
	public ArrayList<Double> getValues(int ppm){
		return matrix.get(ppm);
	}
}
