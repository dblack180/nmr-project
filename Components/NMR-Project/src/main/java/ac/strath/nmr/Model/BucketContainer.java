package ac.strath.nmr.Model;

import java.util.ArrayList;
import java.util.List;

public class BucketContainer {

	private List<PeakBucket> buckets;
	private int spectraID; 

	public BucketContainer(int spectraID){
		this.spectraID = spectraID;
		buckets = new ArrayList<PeakBucket>();
	}

	public int getSpectraID(){
		return this.spectraID;
	}

	public List<PeakBucket> getBuckets(){
		return this.buckets;
	}

	public void addBucket(PeakBucket bucket){
		this.buckets.add(bucket);
	}

	public int getNumPeaks(){
		int n = 0;
		for(PeakBucket pb : buckets){
			n = n + pb.getNumPeaks();
		}

		return n;
	}
}
