package ac.strath.nmr.Model;

import java.util.ArrayList;

public class CorrelationRange {

	ArrayList<CorrelationPeak> peaks;
	double ppmRange;
	double ppmRangeHigh;

	public CorrelationRange(int ppmRange){
		peaks = new ArrayList<CorrelationPeak>();
		this.ppmRange = ppmRange;
		this.ppmRangeHigh = Double.MIN_VALUE;
	}
	
	public double getPPMLow(){
		return this.ppmRange;
	}
	
	public double getPPMHigh(){
		return this.ppmRangeHigh;
	}

	public CorrelationRange(){
		peaks = new ArrayList<CorrelationPeak>();
		this.ppmRange = Double.MAX_VALUE;
		this.ppmRangeHigh = Double.MIN_VALUE;
	}

	public CorrelationRange(int ppmRange, ArrayList<CorrelationPeak> peaks){
		this.peaks = peaks;
		this.ppmRange = ppmRange;
		this.ppmRangeHigh = Double.MIN_VALUE;
	}

	public ArrayList<CorrelationPeak> getPeaks(){
		return this.peaks;
	}

	public double getPPMRange(){
		return this.ppmRange;
	}

	private boolean containsSpectraRecord(int specID){
		for(CorrelationPeak cp : peaks){
			if(cp.getSpectrumID() == specID){
				return true;
			}
		}
		return false;
	}
	
	public void addPeak(CorrelationPeak cp){
		if(!(this.containsSpectraRecord(cp.getSpectrumID()))){
			this.peaks.add(cp);
			if(cp.getPPMRef() <= this.ppmRange){
				this.ppmRange = cp.getPPMRef();	
			}else if(cp.getPPMRef() >= this.ppmRangeHigh){
				this.ppmRangeHigh = cp.getPPMRef();
			}
			
		}
	}

	public void removePeak(CorrelationPeak cp){
		this.peaks.remove(cp);
	}

	public boolean containsZeros() {

		for(CorrelationPeak cp : peaks){
			if(cp.getPPMRef() < 1.0){
				return true;
			}
		}

		return false;
	}
}
