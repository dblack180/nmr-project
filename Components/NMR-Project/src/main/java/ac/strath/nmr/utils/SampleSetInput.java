package ac.strath.nmr.utils;

import javax.swing.*;
import java.awt.event.*;
import java.awt.*;
import java.util.*;


public class SampleSetInput extends JPanel
implements ActionListener {
	JButton selection;

	JFileChooser chooser;
	String choosertitle;

	public SampleSetInput() {
		selection = new JButton("Select Samples Data Set");
		selection.addActionListener(this);
		add(selection);
	}

	public void actionPerformed(ActionEvent e) {
		int result;

		chooser = new JFileChooser(); 
		chooser.setCurrentDirectory(new java.io.File("."));
		chooser.setDialogTitle(choosertitle);
		chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		chooser.setAcceptAllFileFilterUsed(false);
		//    
		if (chooser.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) { 
			System.out.println("getCurrentDirectory(): " 
					+  chooser.getCurrentDirectory());
			System.out.println("getSelectedFile() : " 
					+  chooser.getSelectedFile());
		}
		else {
			System.out.println("No Selection ");
		}
	}

	public Dimension getPreferredSize(){
		return new Dimension(200, 60);
	}

	public static void main(String s[]) {
		JFrame frame = new JFrame("");
		SampleSetInput panel = new SampleSetInput();
		frame.addWindowListener(
				new WindowAdapter() {
					public void windowClosing(WindowEvent e) {
						System.exit(0);
					}
				}
				);
		frame.getContentPane().add(panel,"Center");
		frame.setSize(panel.getPreferredSize());
		frame.setVisible(true);
	}
}