package ac.strath.nmr.Model;

public class CorrelationPeak {

	int spectrumID;
	double intensity;
	double ppmRef;
	
	public CorrelationPeak(int spectrumID, double intensity, double ppmRef){
		this.spectrumID = spectrumID;
		this.intensity = intensity;
		this.ppmRef = ppmRef;
	}
	
	public int getSpectrumID(){
		return this.spectrumID;
	}
	
	public double getIntensity(){
		return this.intensity;
	}
	
	public double getPPMRef(){
		return this.ppmRef;
	}
}
