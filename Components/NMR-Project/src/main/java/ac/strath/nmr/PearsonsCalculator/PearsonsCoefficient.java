package ac.strath.nmr.PearsonsCalculator;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Comparator;
import java.util.List;
import java.util.Random;

import ac.strath.nmr.Model.CorrelationPeak;
import ac.strath.nmr.Model.CorrelationRange;

public class PearsonsCoefficient implements Comparable<PearsonsCoefficient>{

	String dirPrefix = "data/correlations/";
	String fileExt = ".csv";
	CorrelationRange fstVec;
	CorrelationRange sndVec;
	double eucDistance;
	String EDCode;
	Random rand;
	int randID;

	public PearsonsCoefficient(CorrelationRange p1, CorrelationRange p2, double distance){
		this.fstVec = p1;
		this.sndVec = p2;
		this.eucDistance = round(distance, 2);
		rand = new Random();
		randID = rand.nextInt(1001); 
		this.EDCode = this.eucDistance + ":" + this.fstVec.getPeaks().size() + ":" + this.sndVec.getPeaks().size() + ":" + randID; 
		this.writeFile();
	}
	
	public double getEuclidianDistance(){
		return this.eucDistance;
	}

	public CorrelationRange getFirstVector(){
		return this.fstVec;
	}

	public CorrelationRange getSecondVector(){
		return this.sndVec;
	}

	public String getEDCode(){
		return EDCode;
	}

	public int getID(){
		return this.randID;
	}


	@Override
	public int compareTo(PearsonsCoefficient o) {
		final int BEFORE = -1;
		final int EQUAL = 0;
		final int AFTER = 1;

		if(this.getEuclidianDistance() < o.getEuclidianDistance()){
			return BEFORE;
		}else if(this.getEuclidianDistance() > o.getEuclidianDistance()){
			return AFTER;
		}else{
			return EQUAL;
		}
	}

	public static Comparator<PearsonsCoefficient> PearsonsCoefficientComparator 
	= new Comparator<PearsonsCoefficient>() {

		public int compare(PearsonsCoefficient pc1, PearsonsCoefficient pc2) {
			return pc1.compareTo(pc2);
		}

	};

	public static double round(double value, int places) {
		if (places < 0) throw new IllegalArgumentException();

		BigDecimal bd = new BigDecimal(value);
		bd = bd.setScale(places, RoundingMode.HALF_UP);
		return bd.doubleValue();
	}
	
	private void cleanUp(String fileName){
		File file = new File(fileName);
		if(file.exists()){
			file.delete();
			System.out.println("Clean up removed " + file.getAbsolutePath());
		}
	}
	
	private void writeFile(){
		try {
			this.cleanUp(this.dirPrefix + this.EDCode + fileExt);
			FileWriter fr = new FileWriter(this.dirPrefix + this.EDCode + fileExt);
			BufferedWriter br = new BufferedWriter(fr);
			
			for(CorrelationPeak cp : this.fstVec.getPeaks()){
				String formedString = "F" + "," + cp.getSpectrumID() + "," + cp.getPPMRef() + "," + cp.getIntensity() + "," + this.getEuclidianDistance() + "\n"; 
				br.write(formedString);
			}
			
			for(CorrelationPeak cp : this.sndVec.getPeaks()){
				String formedString = "S" + "," + cp.getSpectrumID() + "," + cp.getPPMRef() + "," + cp.getIntensity() + "," + this.getEuclidianDistance() + "\n"; 
				br.write(formedString);
			}
			
			br.close();
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
