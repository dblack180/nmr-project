import sys
from PyQt4 import QtGui
import numpy as np
from peakretrieval import PeakRetriever

import xml.etree.ElementTree as ET
import matplotlib as mpl
from mpl_toolkits.mplot3d import Axes3D
from matplotlib.backends.backend_qt4agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.backends.backend_qt4agg import NavigationToolbar2QTAgg as NavigationToolbar
import matplotlib.pyplot as plt

import random

class Window(QtGui.QDialog):
    def __init__(self, parent=None):
        super(Window, self).__init__(parent)

        # a figure instance to plot on
        self.figure = plt.figure()

        # this is the Canvas Widget that displays the `figure`
        # it takes the `figure` instance as a parameter to __init__
        self.canvas = FigureCanvas(self.figure)

        # this is the Navigation widget
        # it takes the Canvas widget and a parent
        self.toolbar = NavigationToolbar(self.canvas, self)

        # Just some button connected to `plot` method
        self.button = QtGui.QPushButton('Plot')
        self.button.clicked.connect(self.plot)

        # set the layout
        layout = QtGui.QVBoxLayout()
        layout.addWidget(self.toolbar)
        layout.addWidget(self.canvas)
        layout.addWidget(self.button)
        self.setLayout(layout)

    def plot(self):
        ''' plot some random stuff '''
        # random data
        X = [random.random() for i in range(10)]
        Y = [random.random() for i in range(10)]
        Z = [random.random() for i in range(10)]

        # create an axis
        mpl.rcParams['toolbar'] = 'None'
        ax = self.figure.add_subplot(111, projection='3d')

        # discards the old graph
        ax.hold(False)

        tdp = ThreeDimensionalPlotter(ax)

        # refresh canvas
        self.canvas.draw()

class ThreeDimensionalPlotter():
    def __init__(self, plotter):
        pr = PeakRetriever()
        paths = pr.getPaths()
        paths = self.sortPathFiles(paths)
        i = 0

        for p in paths:
            X, Y, Z = [], [], []
            print p
            fo = open(str(p), "rw+")
            tree = ET.parse(p)
            root = tree.getroot()
            for peak in root.iter('Peak1D'):
                X.append(float(peak.attrib.get('F1')))
                Y.append(float(peak.attrib.get('intensity')))
                Z.append(i)

            plotter.plot(X, Z, Y, alpha=0.5, zorder=1, lw=1)
            i+=0.01

        plotter.set_xlabel('X')
        plotter.set_ylabel('Z')
        plotter.set_zlabel('Y')
        plotter.autoscale(True)
        plotter.set_xscale('linear')
        plotter.set_yscale('linear')
        plotter.set_zscale('linear')
        plotter.set_zmargin(0.01)

        fo.close()


    def getAxesVectors(self):
        return self.axesSet

    def sortPathFiles(self, pathSet):
        new_paths = [None] * len(pathSet)
        for path in pathSet:
            tokens = path.split("/")
            index = int(tokens[4]) - 2
            new_paths[index] = path

        cleanPaths = []
        for p in new_paths:
            if p is not None:
                cleanPaths.append(p)
        return cleanPaths

if __name__ == '__main__':
    app = QtGui.QApplication(sys.argv)

    main = Window()
    main.show()

    sys.exit(app.exec_())