class SubtrateCoord:
	def __init__(self, timePt, concentration):
		self.timePt = timePt
		self.concentration = concentration

	def __repr__(self):
		return "%d, %f" % (self.getTimePt(), self.getConcentration())

	def getTimePt(self):
		return self.timePt

	def getConcentration(self):
		return self.concentration
