import xml.etree.ElementTree as ET
from mpl_toolkits.mplot3d import Axes3D
from matplotlib.figure import Figure
from peakretrieval import PeakRetriever
import matplotlib.pyplot as plt
import numpy as np

class MPL2DNMR():
	def __init__(self):
		print "Starting Matplotlib 2d NMR"

	def getTimeframeCount(self):
		return self.timeframeCount

	def sortPathFiles(self, pathSet):
		new_paths = [None] * len(pathSet)
		for path in pathSet:
			tokens = path.split("/")
			index = int(tokens[4]) - 2
			new_paths[index] = path
			final_path = ""
			final_paths = []

		cleanPaths = []
		for p in new_paths:
			if p is not None:
				cleanPaths.append(p)
				final_path = p

		final_paths.append(final_path)
		for p in cleanPaths:
			if p != final_path:
				final_paths.append(p)

		return final_paths
		
	def getFigure(self):
		return self.fig

	def getTimeFrame(self, timeframe):
		print "Trying to load timeframe %d" % timeframe
		pr = PeakRetriever()
		paths = pr.getPaths()
		paths = self.sortPathFiles(paths)
		path = paths[timeframe]

		X, Y = [], []

		fo = open(str(path), "rw+")
		tree = ET.parse(path)
		root = tree.getroot()
		for peak in root.iter('Peak1D'):
			X.append(float(peak.attrib.get('F1')))
			Y.append(float(peak.attrib.get('intensity')))

		fo.close()

		return {'X': X, 'Y': Y}

	def startPlotting(self):
		pr = PeakRetriever()
		paths = pr.getPaths()
		paths = self.sortPathFiles(paths)
		self.timeframeCount = len(paths)
		self.fig = Figure()
		self.ax = self.fig.add_subplot(111)
		# ax2 = self.fig.add_subplot(222)
		self.ax.get_xaxis().get_major_formatter().set_useOffset(False)
		self.ax.patch.set_alpha(1)
		# ax2.get_xaxis().get_major_formatter().set_useOffset(False)
		# ax2.patch.set_alpha(1)

		i = 0
		j = 0.01

		for p in paths:
			X1, Y1, Z1 = [], [], []
			X2, Y2, Z2 = [], [], []
			print p
			fo = open(str(p), "rw+")
			tree = ET.parse(p)
			root = tree.getroot()
			for peak in root.iter('Peak1D'):
				l = 0.005
				for n in range(0,2):
					X1.append(float(peak.attrib.get('F1')) + j)
					Y1.append(float(peak.attrib.get('intensity')) + i)
					Z1.append(i)

			self.lines = self.ax.plot(X1, Y1, alpha=0.8, zorder=1, linewidth=0.5)
			# ax2.plot(X2, Y2, alpha=0.8, zorder=1, linewidth=0.5)

			i+=0.1
			j+=0.01

		self.ax.set_xlabel('ppm')
		self.ax.set_ylabel('intensity')
		self.ax.autoscale(True)
		self.ax.set_xscale('linear')
		self.ax.set_yscale('linear')

		fo.close()