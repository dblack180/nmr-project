from PearsonsProductMC import PearsonsProductMC
from mpl_toolkits.mplot3d import Axes3D
from matplotlib.figure import Figure
from peakretrieval import PeakRetriever
from SubtrateCoord import SubtrateCoord
import xml.etree.ElementTree as ET
import matplotlib.pyplot as plt
import sys

def sortPathFiles(pathSet):
	new_paths = [None] * len(pathSet)
	for path in pathSet:
		tokens = path.split("/")
		index = int(tokens[4]) - 2
		new_paths[index] = path
		final_path = ""
		final_paths = []

	cleanPaths = []
	for p in new_paths:
		if p is not None:
			cleanPaths.append(p)
			final_path = p

	final_paths.append(final_path)
	for p in cleanPaths:
		if p != final_path:
			final_paths.append(p)

	return final_paths


fig = plt.figure()
ax = fig.add_subplot(111)

# pr = PeakRetriever()
# paths = pr.getPaths()
# paths = sortPathFiles(paths)

# i = 0

# for p in paths:
# 	X, Y, Z = [], [], []
# 	print p
# 	fo = open(str(p), "rw+")
# 	tree = ET.parse(p)
# 	root = tree.getroot()
# 	for peak in root.iter('Peak1D'):
# 		X.append(float(peak.attrib.get('F1')))
# 		Y.append(float(peak.attrib.get('intensity')))
# 		Z.append(i)

X = [
0
,4
,19
,23
,34
,35
,36
,37
,39
,40
,41
,42
,43
,44
,45
,46
,47
,48
,49
,50
,51
,52
,53
,54
,55
,56
,57
,58
,59
,60
,63]
Y = [
0.12 
,0.15 
,0.16
,0.18
,0.25
,0.21
,0.25
,0.28
,0.28
,0.3
,0.34
,0.32
,0.33
,0.33
,0.3
,0.31
,0.29
,0.25
,0.27
,0.28
,0.25
,0.26
,0.26
,0.24
,0.25
,0.21
,0.2
,0.19
,0.19
,0.17
,0.17
,0.14]


subtrate_list = []

print len(X)
print len(Y)

i = 0
for x in X:
	subtrate_list.append(SubtrateCoord(x, Y[i]))
	i+=1

for sc in subtrate_list:
	print sc

for i in range(0, 63):
	


# max_V = (1 - sys.maxint)
# Y.sort()
# YFixed = [] 

# for v in Y:
# 	if v > max_V:
# 		max_V = v

# print "Maximum Velocity: %f" % max_V
# print "Km constant: %f" % (max_V / 2)

# Km_constant = (max_V / 2)
# maxV_divi = (max_V / 64)

# for sc in Y:
	# subtrate_C = (max_V * sc) / (Km_constant + sc)
	# YFixed.append(subtrate_C)


# ax.plot(X, YFixed, alpha=0.8, zorder=1, linewidth=0.5)

# plt.show()

# ppmc = PearsonsProductMC()
# r_val = ppmc.calculate_R()
