import xml.etree.ElementTree as ET
import pylab
from mpl_toolkits.mplot3d import Axes3D
from peakretrieval import PeakRetriever
import matplotlib.ticker as ticker
import matplotlib.pyplot as plt
from matplotlib import cm
import numpy as np
import sys
import os

class MPL3DNMR():
	def __init__(self):
		print "Starting 3D Visualisation"
		#print "FILENAME %s" % filename[0]
		#self.filename = filename[0]
	def getTimeframeCount(self):
		return self.timeframeCount

	def sortPathFiles(self, pathSet):
		new_paths = [None] * len(pathSet)
		for path in pathSet:
			tokens = path.split("/")
			index = int(tokens[4]) - 2
			new_paths[index] = path
			final_path = ""
			final_paths = []

		cleanPaths = []
		for p in new_paths:
			if p is not None:
				cleanPaths.append(p)
				final_path = p

		final_paths.append(final_path)
		for p in cleanPaths:
			if p != final_path:
				final_paths.append(p)

		return final_paths

	def startPlotting(self):
		pr = PeakRetriever("Rattlesnake_YGG")
		paths = pr.getPaths()
		paths = self.sortPathFiles(paths)
		fig = pylab.figure()
		ax = Axes3D(fig)
		ax.cla()
		ax.autoscale(enable=True, axis=u'both', tight=None)
		ax.mouse_init(rotate_btn=1, zoom_btn=3)
		ax.patch.set_alpha(1)
		scale = 1e-9
		i = 0

		'''
		fvp = pr.getCorrPaths("../Components/NMR-Project-3.0/data/correlations/" + self.filename, "F")
		fvpX = fvp['X']
		fvpY = fvp['Y']
		fvpZ = fvp['Z']

		xAnnotA = fvpX[len(fvpX)/2]
		yAnnotA = fvpY[len(fvpY)/2]
		zAnnotA = fvpZ[len(fvpZ)/2]

		svp = pr.getCorrPaths("../Components/NMR-Project-3.0/data/correlations/" + self.filename, "S")
		svpX = svp['X']
		svpY = svp['Y']
		svpZ = svp['Z']
		svpD = svp['D']
		svpD = svpD[0]

		ax.set_title("Correlation Value: %f" % svpD, fontdict=None, loc=u'center')

		xAnnotB = svpX[len(svpX)/2]
		yAnnotB = svpY[len(svpY)/2]
		zAnnotB = svpZ[len(svpZ)/2]
		'''

		for p in paths:
		    X, Y, Z = [], [], []
		    print p
		    fo = open(str(p), "rw+")
		    tree = ET.parse(p)
		    root = tree.getroot()
		    for peak in root.iter('Peak1D'):
		        X.append(float(peak.attrib.get('F1')))
		        Y.append(float(peak.attrib.get('intensity')))
		        Z.append(i)


		    # ax.text(xAnnotA, 0.1, zAnnotA, "Correlation A", color='red')
		    # ax.text(xAnnotB, 0.1, zAnnotB, "Correlation B", color='blue')
		    ax.plot(X, Z, Y, alpha=0.5, zorder=1, lw=1)
		    i+=0.01

		# for i in range(0, len(svpZ)):
		# 	ax.scatter(svpX[i],svpZ[i],svpY[i], c='b', marker='o')

		ax.set_xlabel('ppm')
		ax.set_ylabel('spectrum')
		ax.set_zlabel('intensity')
		ax.set_xscale('linear')
		ax.set_yscale('linear')
		ax.set_zscale('linear')

		fo.close()

		ax.invert_xaxis()

		ax.axis("off")
		ax.set_zlim3d(0.0, 64)
		ax.set_xlim3d(0.0, 12.0)
		ax.set_ylim3d(0.0, 30)
		ax.grid(b=False)
		ax.set_axisbelow(b=False)
		ax.set_autoscalez_on(True)
		ax.autoscale(enable=True, axis=u'both', tight=False)
		ax.relim(visible_only=True)
		ax.autoscale_view(tight=False, scalex=True, scaley=True, scalez=True)

		# fileList = os.listdir('Panorama2/')
		# for filename in fileList:
		# 	os.remove('Panorama2' + "/" + filename)
		# for ii in xrange(0,360,1):
		# 	ax.view_init(elev=10., azim=ii)
		# 	plt.savefig("Panorama2/%d.png" % ii)

		# for jj in xrange(0,360,1):
		# 	ax.view_init(elev=10., azim=jj)
		# 	plt.savefig("Panorama/%d.png" % (360 + jj))

		pylab.show()

if __name__ == "__main__":
	filename = sys.argv[1:]
	mpl = MPL3DNMR()
	mpl.startPlotting()
