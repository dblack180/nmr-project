from peakretrieval import PeakRetriever

class NMRFileLoader():
	def __init__(self):
		self.DELIMITER = ','
		self.pr = PeakRetriever()
		self.paths = self.pr.getPaths()
		self.pr.writePeakFiles()
		self.base_path = self.pr.get_stored_path()
		self.no_of_spectra = len(self.paths)
		self.xs, self.ys, self.zs = [], [], []

	def get_contents_of_file(self, spectrumID):
		try:
			if (spectrumID <= self.no_of_spectra - 1):
				self.clear_sets()
				with open('%s%d.csv' % (self.base_path, spectrumID)) as fp:
					for line in iter(fp.readline, ''):
						tokens = line.split(self.DELIMITER)
						self.xs.append(float(tokens[0]))
						self.ys.append(float(tokens[1]))
						self.zs.append(float(spectrumID - 1))

		except Exception:
			print "No spectrum file matching supplied spectrumID."

	def clear_sets(self):
		self.xs, self.ys, self.zs = [], [], []

	def get_xs(self):
		return self.xs

	def get_ys(self):
		return self.ys

	def get_zs(self):
		return self.zs

# nmr = NMRFileLoader()
# nmr.get_contents_of_file(0)
# print nmr.xs