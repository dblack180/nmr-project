import os
import pprint
import random
import sys
import wx

# The recommended way to use wx with mpl is with the WXAgg
# backend. 
#
import matplotlib
matplotlib.use('WXAgg')
from matplotlib.figure import Figure
import matplotlib.pyplot as plt
from matplotlib.backends.backend_wxagg import \
    FigureCanvasWxAgg as FigCanvas, \
    NavigationToolbar2WxAgg as NavigationToolbar
import numpy as np
import pylab
from matplotlibpoc2d import MPL2DNMR

class NMRDashboard(wx.Frame):
    def __init__(self, parent, title):
        wx.Frame.__init__(self, parent, title=title, size=(500,400))

        mainSizer = wx.BoxSizer(wx.VERTICAL)
        grid = wx.GridBagSizer(hgap=5, vgap=5)
        hSizer = wx.BoxSizer(wx.HORIZONTAL)

        filemenu= wx.Menu()
        menuAbout= filemenu.Append(wx.ID_ABOUT, "&About"," Information about this program")
        menuLoad = filemenu.Append(wx.ID_OPEN, "&Load", " Load NMR Peak Files")
        menuSave = filemenu.Append(wx.ID_SAVE, "&Save", " Save NMR Workspace")

        menuBar = wx.MenuBar()
        menuBar.Append(filemenu, "&File")
        self.SetMenuBar(menuBar)

        self.Bind(wx.EVT_MENU, self.aboutDialog, menuAbout)
        self.Bind(wx.EVT_MENU, self.loadNMRFiles, menuLoad)
        self.Bind(wx.EVT_MENU, self.saveNMRFiles, menuSave)

        self.init_plotting()

        self.panel = wx.Panel(self)
        self.canvas = FigCanvas(self.panel, -1, self.nmrPlt.getFigure())

        self.CreateStatusBar()
        self.Show() 

    def saveNMRFiles(self, e):
        self.SetStatusText("Saving..", 0)

    def loadNMRFiles(self, e):
        self.SetStatusText("Loading NMR Files..", 0)

    def aboutDialog(self, e):
        self.SetStatusText("Reading about", 0)
        dlg = wx.MessageDialog(self, "Analysis and visualisation tool for \nNuclear Magnetic Resonance Spectroscopy\nFinal Year Project of David Black\nStrathclyde\nComputer Science", "Strathclyde NMR v1.0", wx.OK)
        dlg.ShowModal()
        dlg.Destroy()

    def init_plotting(self):
        self.nmrPlt = MPL2DNMR()
        self.nmrPlt.startPlotting()


app = wx.App(False)
frame = NMRDashboard(None, "NMR Dash")
app.MainLoop()