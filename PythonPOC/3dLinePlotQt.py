from peakretrieval import PeakRetriever
from pyqtgraph.Qt import QtCore, QtGui
from PyQt4 import *
import xml.etree.ElementTree as ET
import pyqtgraph.opengl as gl
import pyqtgraph as pg
from pyqtgraph import *
import numpy as np

app = QtGui.QApplication([])

w = gl.GLViewWidget()
w.opts['distance'] = 40
w.show()
w.setWindowTitle('NMR Data Example')

gx = gl.GLGridItem()
gx2 = gl.GLGridItem()
xAx = AxisItem(orientation='bottom')
xAx.setGrid(gx)
xAx.setLabel('PPM')
gx.rotate(0, 0, 1, 0)
gx.translate(10, 1, 0)
w.addItem(gx)
gy = gl.GLGridItem()
gy.rotate(90, 1, 0, 0)
gy.translate(10, 11, 10)
w.addItem(gy)

def sortPathFiles(pathSet):
    new_paths = [None] * len(pathSet)
    for path in pathSet:
        tokens = path.split("/")
        index = int(tokens[4]) - 2
        new_paths[index] = path

    cleanPaths = []
    for p in new_paths:
        if p is not None:
            cleanPaths.append(p)
    return cleanPaths

pr = PeakRetriever()
paths = pr.getPaths()
paths = sortPathFiles(paths)

i = 0
n = 0
for p in paths:
    X, Y, Z = [], [], []
    print p
    fo = open(str(p), "rw+")
    tree = ET.parse(p)
    root = tree.getroot()
    for peak in root.iter('Peak1D'):
            X.append(float(peak.attrib.get('F1')))
            Y.append(float(peak.attrib.get('intensity')))
            Z.append(i)

    i+=0.3
    n+=1
    pts = np.vstack([Z,X,Y]).transpose()
    plt = gl.GLLinePlotItem(pos=pts, color=pg.glColor(1000/n), width=0.5, antialias=True, mode='line_strip')
    w.addItem(plt)
    


## Start Qt event loop unless running in interactive mode.
if __name__ == '__main__':
    import sys
    if (sys.flags.interactive != 1) or not hasattr(QtCore, 'PYQT_VERSION'):
        QtGui.QApplication.instance().exec_()
