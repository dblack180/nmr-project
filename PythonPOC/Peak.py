class Peak:
	def __init__(self, uid, sample, ppm, concentration, spectrumID):
		self.uid = uid
		self.sample = sample
		self.ppm = ppm
		self.concentration = concentration
		self.spectrumID = spectrumID
	
	def __lt__(self, other):
		return self.ppm < other.ppm

	def __repr__(self):
		return "%s,%s,%s,%s,%s\n" % (self.uid, self.sample, self.ppm, self.concentration, self.spectrumID)

	def getUID():
		return self.uid

	def getSample():
		return self.sample

	def getPPM():
		return self.ppm

	def getConcentration():
		return self.concentration
	
	def getSpectrumID():
		return self.spectrumID

