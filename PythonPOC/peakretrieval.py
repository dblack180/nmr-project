import codecs
import os
import sys
from sets import Set
import xml.etree.ElementTree as ET
from Peak import Peak
'''
Perform OS walk through directory of nmr data to build a set of 
absolute paths to peak files.
'''

class PeakRetriever:
	def __init__(self, dirName):
		self.all_paths = Set([])
		self.pf_paths = Set([])
		self.crawlQueue = Set([])
		self.discovered = Set([])
		self.trackedDirectory = ""
		self.targetFile = "peaklist.xml"
		self.base_path = "../DataSet/Archive/%s" % dirName
		self.stored_path = ""
		self.crawlQueue.add(self.base_path)
		self.DFS()
		self.cleanUp()
		self.writePeakRange()

	def getCorrPaths(self, filename, vect):
		X = []
		Y = []
		Z = []
		D = []

		with open(filename, "r") as fo:
			for lines in fo:
				tokens = lines.split(",")
				if(tokens[0] is vect):
					Z.append(int(tokens[1]))
					X.append(float(tokens[2]))
					Y.append(float(tokens[3]))
					D.append(float(tokens[4]))

		return {"X": X, "Y": Y, "Z": Z, "D": D}

	def DFS(self):
		while len(self.crawlQueue) > 0:
			curr = self.crawlQueue.pop()
			self.base_path = curr
			for root, dirs, files in os.walk(curr):
				for d in dirs:
					self.all_paths.add(self.base_path + "/" + d)
					self.crawlQueue.add(self.base_path + "/" + d)
					for root, dirs, files in os.walk(self.base_path + "/" + d):
						for f in files:
							if f == self.targetFile:
								self.all_paths.add(self.base_path + "/" + d + "/" + self.targetFile)

	def cleanUp(self):
		for path in self.all_paths:
			if ("pdata/1/" + self.targetFile) in path:
				self.pf_paths.add(path)

	def printAll(self):
		for path in self.pf_paths:
			print path

	def getPaths(self):
		return self.pf_paths

	def sortPathFiles(self, pathSet):
		new_paths = [None] * len(pathSet)
		for path in pathSet:
			tokens = path.split("/")
			index = int(tokens[4]) - 2
			new_paths[index] = path
			last_path = ""

		cleanPaths = []
		finalPaths = []
		for p in new_paths:
			if p is not None:
				cleanPaths.append(p)
				last_path = p

		finalPaths.append(last_path)
		for p in cleanPaths:
			if p != last_path:
				finalPaths.append(p)



		return finalPaths

	def set_stored_path(self, path):
		self.stored_path = path

	def get_stored_path(self):
		return self.stored_path

	def writePeakFiles(self):
		paths = self.getPaths()
		paths = self.sortPathFiles(paths)
		self.set_stored_path('../DataSet/SortedPeakFiles/')
		i = 0
		for p in paths:
			X, Y, Z = [], [], []
			fo = open(str(p), "rw+")
			fw = open('../DataSet/SortedPeakFiles/%d.csv' % i, 'w')
			tree = ET.parse(p)
			root = tree.getroot()
			for peak in root.iter('Peak1D'):
				x = (float(peak.attrib.get('F1')))
				y = (float(peak.attrib.get('intensity')))
				z = i
				fw.write('%f,%f,%f\n' % (x, y, z))

			fo.close()
			fw.close()
			i+=1
		print "Sorted peak files have been written to disk"

	def writePeakRange(self):
		self.max_x = (1 - sys.maxint)
		self.min_x = sys.maxint
		paths = self.getPaths()
		paths = self.sortPathFiles(paths)

		self.initMinMaxPPM(paths[0])

		j = 0
		uid = 0
		test_list = []
		for i in range(int(self.min_x), int(self.max_x)+1):
			for p in paths:
				fo = open(str(p), "rw+")
				tree = ET.parse(p)
				root = tree.getroot()
				for peak in root.iter('Peak1D'):
					if i == (float(peak.attrib.get('F1'))):
						x = (float(peak.attrib.get('F1')))
						y = (float(peak.attrib.get('intensity')))
						z = j
						test_list.append(Peak(uid, "Rattlesnake venom", str(x), str(y), str(z)))
						uid+=1

				j+=1
				fo.close()
			j=0

		test_list.sort()

		with codecs.open('../DataSet/PeakRanges/all_peaks.csv', 'a', 'utf-8') as write_file:
			write_file.write("UID,Sample,PPM,Concentration,Spectrum_ID\n")
			for peak in test_list:
				write_file.write(str(peak))

	def initMinMaxPPM(self, path):
		fo = open(str(path), "rw+")
		tree = ET.parse(path)
		root = tree.getroot()
		for peak in root.iter('Peak1D'):
			x = (float(peak.attrib.get('F1')))
			if x > self.max_x:
				self.max_x = x
			elif x < self.min_x:
				self.min_x = x

		fo.close()

pr = PeakRetriever("C03519")
pr.writePeakFiles()