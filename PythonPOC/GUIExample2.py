import sys, os, random
from PyQt4.QtCore import *
from PyQt4.QtGui import *

import matplotlib
from matplotlib.backends.backend_qt4agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.backends.backend_qt4agg import NavigationToolbar2QT as NavigationToolbar
from matplotlib.figure import Figure
from matplotlibpoc2d import MPL2DNMR
from matplotlibpoc3d import MPL3DNMR

class AppForm(QMainWindow):
    def __init__(self, parent=None):
        QMainWindow.__init__(self, parent)
        self.setWindowTitle('Strathclyde NMR')

        self.create_menu()
        self.create_main_frame()
        self.create_status_bar()
        self.current_timeframe = 0
        self.total_timeframes = 0
        self.increment_number = 0.00

    def increment_timeframe(self):
        self.total_timeframes = self.nmr.getTimeframeCount()
        if self.current_timeframe < self.total_timeframes:
            self.current_timeframe+=1
            self.redrawSpectra("increment")
            self.statusBar().showMessage('Displaying 2D timeframe %d' % self.current_timeframe, 2000)

    def decrement_timeframe(self):
        self.total_timeframes = self.nmr.getTimeframeCount()
        if self.current_timeframe > 0:
            self.current_timeframe-=1
            self.redrawSpectra("decrement")
            self.statusBar().showMessage('Displaying 2D timeframe %d' % self.current_timeframe, 2000)

    def redrawSpectra(self, command=None):
        if command is not None:
            if command is "increment" or command is "decrement":
                newAxes = self.nmr.getTimeFrame(self.current_timeframe)
                self.nmr.fig.set_canvas(self.canvas)
                self.nmr.ax.clear()
                self.nmr.ax.plot(newAxes['X'], newAxes['Y'], alpha=0.8, zorder=1, linewidth=0.8)
                self.canvas.draw()  
            elif command is "selectTimeframe":
                newAxes = self.nmr.getTimeFrame(self.current_timeframe)
                self.nmr.fig.set_canvas(self.canvas)
                self.nmr.ax.clear()
                self.nmr.ax.plot(newAxes['X'], newAxes['Y'], alpha=0.8, zorder=1, linewidth=0.8)
                self.canvas.draw()

    def save_plot(self):
        file_choices = "PNG (*.png)|*.png"
        
        path = unicode(QFileDialog.getSaveFileName(self, 
                        'Save file', '', 
                        file_choices))
        if path:
            self.canvas.print_figure(path, dpi=self.dpi)
            self.statusBar().showMessage('Saved to %s' % path, 2000)
    
    def on_about(self):
        msg = """Analysis and visualisation tool for
                 Nuclear Magnetic Resonance Spectroscopy
                 Final Year Project of David Black
                 Strathclyde, Computer Science"
                 Strathclyde NMR v1.0
            """
        QMessageBox.about(self, "About", msg.strip())
    
    def process_input(self):
        print "Command given: %s" % (self.textbox.text())
        uInput = self.textbox.text().split(' ')
        if uInput[0] == "tf":
            print "Timeframe selection to TF: %d" % (int(uInput[1]))
            self.current_timeframe = int(uInput[1])
            self.redrawSpectra("selectTimeframe")

    def on_pick(self, event):
        box_points = event.artist.get_bbox().get_points()
        msg = "You've clicked on a bar with coords:\n %s" % box_points
        
        QMessageBox.information(self, "Click!", msg)
    
    def load_three_dim_view(self):
        self.threed_nmr = MPL3DNMR()
        self.threed_nmr.startPlotting()
    
    def create_main_frame(self):
        self.main_frame = QWidget()
        self.nmr = MPL2DNMR()
        self.nmr.startPlotting()
        
        self.dpi = 100
        self.fig = self.nmr.getFigure()
        self.canvas = FigureCanvas(self.fig)
        self.canvas.setParent(self.main_frame)

        self.canvas.mpl_connect('pick_event', self.on_pick)
        
        self.mpl_toolbar = NavigationToolbar(self.canvas, self.main_frame)
        
        self.lblInput = QLabel("Show timeframe or view (2D / 3D): ")
        self.textbox = QLineEdit()
        self.textbox.setMinimumWidth(200)
        self.connect(self.textbox, SIGNAL('editingFinished ()'), self.process_input)
        
        self.draw_button = QPushButton("&Draw")
        self.threed_button = QPushButton("&3D View")
        self.increment_button = QPushButton("&+")
        self.decrement_button = QPushButton("&-")
        self.connect(self.threed_button, SIGNAL('clicked()'), self.load_three_dim_view)
        self.connect(self.increment_button, SIGNAL('clicked()'), self.increment_timeframe)
        self.connect(self.decrement_button, SIGNAL('clicked()'), self.decrement_timeframe)
        
        
        self.grid_cb = QCheckBox("Show &Grid")
        self.grid_cb.setChecked(False)
        self.connect(self.grid_cb, SIGNAL('stateChanged(int)'), self.process_input)
        
        slider_label = QLabel('Bar width (%):')
        self.slider = QSlider(Qt.Horizontal)
        self.slider.setRange(1, 100)
        self.slider.setValue(20)
        self.slider.setTracking(True)
        self.slider.setTickPosition(QSlider.TicksBothSides)
        self.connect(self.slider, SIGNAL('valueChanged(int)'), self.process_input)
        
        hbox = QHBoxLayout()
        
        for w in [  self.lblInput, self.textbox, self.threed_button, self.increment_button,
                    self.decrement_button]:
            hbox.addWidget(w)
            hbox.setAlignment(w, Qt.AlignVCenter)
        
        vbox = QVBoxLayout()
        vbox.addWidget(self.canvas)
        vbox.addWidget(self.mpl_toolbar)
        vbox.addLayout(hbox)
        
        self.main_frame.setLayout(vbox)
        self.setCentralWidget(self.main_frame)
    
    def create_status_bar(self):
        self.status_text = QLabel("Displaying 2D Spectra")
        self.statusBar().addWidget(self.status_text, 1)
        
    def create_menu(self):        
        self.file_menu = self.menuBar().addMenu("&File")
        
        load_file_action = self.create_action("&Save plot",
            shortcut="Ctrl+S", slot=self.save_plot, 
            tip="Save the plot")
        quit_action = self.create_action("&Quit", slot=self.close, 
            shortcut="Ctrl+Q", tip="Close the application")
        
        self.add_actions(self.file_menu, 
            (load_file_action, None, quit_action))
        
        self.help_menu = self.menuBar().addMenu("&Help")
        about_action = self.create_action("&About", 
            shortcut='F1', slot=self.on_about, 
            tip='About the demo')
        
        self.add_actions(self.help_menu, (about_action,))

    def add_actions(self, target, actions):
        for action in actions:
            if action is None:
                target.addSeparator()
            else:
                target.addAction(action)

    def create_action(  self, text, slot=None, shortcut=None, 
                        icon=None, tip=None, checkable=False, 
                        signal="triggered()"):
        action = QAction(text, self)
        if icon is not None:
            action.setIcon(QIcon(":/%s.png" % icon))
        if shortcut is not None:
            action.setShortcut(shortcut)
        if tip is not None:
            action.setToolTip(tip)
            action.setStatusTip(tip)
        if slot is not None:
            self.connect(action, SIGNAL(signal), slot)
        if checkable:
            action.setCheckable(True)
        return action


def main():
    app = QApplication(sys.argv)
    form = AppForm()
    form.show()
    app.exec_()


if __name__ == "__main__":
    main()