from NMRFileLoader import NMRFileLoader
import math

class PearsonsProductMC():
	def __init__(self):
		self.nmrFL = NMRFileLoader()
		self.nmrFL.get_contents_of_file(2)

		self.x, self.y, self.xsq, self.ysq, self.xy, self.occ = [], [], [], [], [], 0
		self.populateXY()

	def populateXY(self):
		self.x = self.nmrFL.get_xs()
		self.y = self.nmrFL.get_ys()

	def calculate_mean_of_x(self):
		total = 0;
		for x in self.x:
			total+=x

		mean = total / len(self.x)
		return round(mean, 3)

	def calculate_mean_of_y(self):
		total = 0;
		for y in self.y:
			total+=y

		mean = total / len(self.y)
		return round(mean, 3)

	def calculate_SSx(self):
		sumX = 0
		x_mean = self.calculate_mean_of_x()
		for x in self.x:
			sumX+=(x - x_mean)*(x - x_mean)

		return round(sumX,2)

	def calculate_SSy(self):
		sumY = 0
		y_mean = self.calculate_mean_of_y()
		for y in self.y:
			sumY+=(y - y_mean)*(y - y_mean)

		return round(sumY,3)

	def calculate_x_y_combined(self):
		sumXY = 0
		x_mean = self.calculate_mean_of_x()
		y_mean = self.calculate_mean_of_y()
		for i in range(0, len(self.x)):
			sumXY+=((self.x[i] - x_mean) * (self.y[i] - y_mean))

		return round(sumXY, 3)

	def calculate_R(self):
		r = (self.calculate_x_y_combined() / (math.sqrt(self.calculate_SSx() * self.calculate_SSy())))
		return round(r, 4)

ppmc = PearsonsProductMC()
print ppmc.calculate_mean_of_x()
print ppmc.calculate_mean_of_y()
print ppmc.calculate_SSx()
print ppmc.calculate_SSy()
print ppmc.calculate_x_y_combined()
print ppmc.calculate_R()