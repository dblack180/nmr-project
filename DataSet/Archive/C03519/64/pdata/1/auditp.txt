##TITLE= Audit trail, TOPSPIN		Version 2.1
##JCAMPDX= 5.01
##ORIGIN= Bruker BioSpin GmbH
##OWNER= NMR User
$$ C:\Bruker\TOPSPIN/data/Rose/nmr/C03519/64/pdata/1/auditp.txt
##AUDIT TRAIL=  $$ (NUMBER, WHEN, WHO, WHERE, PROCESS, VERSION, WHAT)
(   1,<2014-12-16 21:13:40.593 +0000>,<Administrator>,<GH001606>,<go>,<TOPSPIN 2.1>,
      <created by zg
	started at 2014-12-16 21:12:25.046 +0000,
	POWCHK disabled, PULCHK disabled,
       configuration hash MD5:
       4F DB FE 01 64 29 F4 D5 05 17 18 DB FF 7C 08 05
       data hash MD5: 32K
       D7 19 DE 73 3B 8B 40 1C 33 22 EE 12 40 56 46 83>)
(   2,<2014-12-16 21:13:54.890 +0000>,<Administrator>,<GH001606>,<audit>,<TOPSPIN 2.1>,
      <user comment:
       ICON-NMR User ID: MJD
       data hash MD5: 32K
       D7 19 DE 73 3B 8B 40 1C 33 22 EE 12 40 56 46 83>)
(   3,<2014-12-16 21:13:56.015 +0000>,<Administrator>,<GH001606>,<dirdata>,<TOPSPIN 2.1>,
      <wrpa from 1 64 "C03519" "MJD" "C:\NMRdata"
       data hash MD5: 32K
       D7 19 DE 73 3B 8B 40 1C 33 22 EE 12 40 56 46 83>)
(   4,<2015-01-30 14:22:49.587 +0000>,<NMR User>,<CHEM-NMR04>,<proc1d>,<TOPSPIN 2.1>,
      <Start of raw data processing
       efp LB = 0.3 FT_mod = 6 PKNL = 1 PHC0 = 202.5637 PHC1 = 0 TDeff = 16K SI = 32K 
       data hash MD5: 32K
       AE 08 B1 E4 C0 3B 72 99 84 1F F0 C0 DF D5 6A 0B>)
(   5,<2015-01-30 16:22:28.994 +0000>,<NMR User>,<CHEM-NMR04>,<proc1d>,<TOPSPIN 2.1>,
      <pk fgphup PHC0 = 40.2625 PHC1 = 0 
       data hash MD5: 32K
       D2 97 DC 64 BB 61 11 31 FA 10 A9 E9 7C 43 3A 21>)
##END=

$$ hash MD5
$$ 4F B7 9B ED F4 72 83 B1 01 83 E4 35 79 8C A2 5F
