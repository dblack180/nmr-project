##TITLE= Audit trail, TOPSPIN		Version 2.1
##JCAMPDX= 5.01
##ORIGIN= Bruker BioSpin GmbH
##OWNER= NMR User
$$ C:\Bruker\TOPSPIN/data/Rose/nmr/C03519/62/pdata/1/auditp.txt
##AUDIT TRAIL=  $$ (NUMBER, WHEN, WHO, WHERE, PROCESS, VERSION, WHAT)
(   1,<2014-12-16 21:05:17.750 +0000>,<Administrator>,<GH001606>,<go>,<TOPSPIN 2.1>,
      <created by zg
	started at 2014-12-16 21:04:02.218 +0000,
	POWCHK disabled, PULCHK disabled,
       configuration hash MD5:
       4F DB FE 01 64 29 F4 D5 05 17 18 DB FF 7C 08 05
       data hash MD5: 32K
       0E E2 E7 26 39 9D F0 86 A2 D1 E7 3C D3 C5 DB DB>)
(   2,<2014-12-16 21:05:30.953 +0000>,<Administrator>,<GH001606>,<audit>,<TOPSPIN 2.1>,
      <user comment:
       ICON-NMR User ID: MJD
       data hash MD5: 32K
       0E E2 E7 26 39 9D F0 86 A2 D1 E7 3C D3 C5 DB DB>)
(   3,<2014-12-16 21:05:32.031 +0000>,<Administrator>,<GH001606>,<dirdata>,<TOPSPIN 2.1>,
      <wrpa from 1 62 "C03519" "MJD" "C:\NMRdata"
       data hash MD5: 32K
       0E E2 E7 26 39 9D F0 86 A2 D1 E7 3C D3 C5 DB DB>)
(   4,<2015-01-30 14:22:48.324 +0000>,<NMR User>,<CHEM-NMR04>,<proc1d>,<TOPSPIN 2.1>,
      <Start of raw data processing
       efp LB = 0.3 FT_mod = 6 PKNL = 1 PHC0 = 196.7516 PHC1 = 0 TDeff = 16K SI = 32K 
       data hash MD5: 32K
       4D 37 2F F1 C2 98 FB 80 DD 43 2D 89 CB D6 44 56>)
(   5,<2015-01-30 16:21:33.130 +0000>,<NMR User>,<CHEM-NMR04>,<proc1d>,<TOPSPIN 2.1>,
      <pk fgphup PHC0 = 41.38973 PHC1 = 5.1 
       data hash MD5: 32K
       8C AA D7 A5 AB 64 31 9F F9 72 77 AA 45 E4 5E 6B>)
##END=

$$ hash MD5
$$ A9 7B BB 0C 6B 9A 0B 9E E1 BA 4C 64 22 F9 14 B6
