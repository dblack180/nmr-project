##TITLE= Audit trail, TOPSPIN		Version 2.1
##JCAMPDX= 5.01
##ORIGIN= Bruker BioSpin GmbH
##OWNER= NMR User
$$ C:\Bruker\TOPSPIN/data/Rose/nmr/C03519/7/pdata/1/auditp.txt
##AUDIT TRAIL=  $$ (NUMBER, WHEN, WHO, WHERE, PROCESS, VERSION, WHAT)
(   1,<2014-12-16 17:16:57.156 +0000>,<Administrator>,<GH001606>,<go>,<TOPSPIN 2.1>,
      <created by zg
	started at 2014-12-16 17:15:41.500 +0000,
	POWCHK disabled, PULCHK disabled,
       configuration hash MD5:
       4F DB FE 01 64 29 F4 D5 05 17 18 DB FF 7C 08 05
       data hash MD5: 32K
       68 C2 F5 12 45 E6 79 91 21 53 FC 28 08 BF C7 10>)
(   2,<2014-12-16 17:17:10.703 +0000>,<Administrator>,<GH001606>,<audit>,<TOPSPIN 2.1>,
      <user comment:
       ICON-NMR User ID: MJD
       data hash MD5: 32K
       68 C2 F5 12 45 E6 79 91 21 53 FC 28 08 BF C7 10>)
(   3,<2014-12-16 17:17:11.781 +0000>,<Administrator>,<GH001606>,<dirdata>,<TOPSPIN 2.1>,
      <wrpa from 1 7 "C03519" "MJD" "C:\NMRdata"
       data hash MD5: 32K
       68 C2 F5 12 45 E6 79 91 21 53 FC 28 08 BF C7 10>)
(   4,<2015-01-30 14:22:13.691 +0000>,<NMR User>,<CHEM-NMR04>,<proc1d>,<TOPSPIN 2.1>,
      <Start of raw data processing
       efp LB = 0.3 FT_mod = 6 PKNL = 1 PHC0 = 196.5706 PHC1 = 0 TDeff = 16K SI = 32K 
       data hash MD5: 32K
       23 38 9E 5A 95 AC AE E5 D9 88 5B 1A 1D 97 6A D0>)
(   5,<2015-01-30 15:49:37.790 +0000>,<NMR User>,<CHEM-NMR04>,<proc1d>,<TOPSPIN 2.1>,
      <pk fgphup PHC0 = 43.44748 PHC1 = 4.6 
       data hash MD5: 32K
       A9 10 BF 68 23 C2 0D A3 3F F5 D4 44 1B F9 72 61>)
##END=

$$ hash MD5
$$ 07 0C 02 47 EC B2 E9 A1 EA E3 9B B5 15 04 99 79
