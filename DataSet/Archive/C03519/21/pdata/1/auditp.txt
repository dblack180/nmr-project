##TITLE= Audit trail, TOPSPIN		Version 2.1
##JCAMPDX= 5.01
##ORIGIN= Bruker BioSpin GmbH
##OWNER= NMR User
$$ C:\Bruker\TOPSPIN/data/Rose/nmr/C03519/21/pdata/1/auditp.txt
##AUDIT TRAIL=  $$ (NUMBER, WHEN, WHO, WHERE, PROCESS, VERSION, WHAT)
(   1,<2014-12-16 18:14:58.453 +0000>,<Administrator>,<GH001606>,<go>,<TOPSPIN 2.1>,
      <created by zg
	started at 2014-12-16 18:13:42.765 +0000,
	POWCHK disabled, PULCHK disabled,
       configuration hash MD5:
       4F DB FE 01 64 29 F4 D5 05 17 18 DB FF 7C 08 05
       data hash MD5: 32K
       9B F9 A5 F1 38 65 11 01 3E 93 60 0F 13 91 F2 9D>)
(   2,<2014-12-16 18:15:12.187 +0000>,<Administrator>,<GH001606>,<audit>,<TOPSPIN 2.1>,
      <user comment:
       ICON-NMR User ID: MJD
       data hash MD5: 32K
       9B F9 A5 F1 38 65 11 01 3E 93 60 0F 13 91 F2 9D>)
(   3,<2014-12-16 18:15:12.640 +0000>,<Administrator>,<GH001606>,<dirdata>,<TOPSPIN 2.1>,
      <wrpa from 1 21 "C03519" "MJD" "C:\NMRdata"
       data hash MD5: 32K
       9B F9 A5 F1 38 65 11 01 3E 93 60 0F 13 91 F2 9D>)
(   4,<2015-01-30 14:22:22.537 +0000>,<NMR User>,<CHEM-NMR04>,<proc1d>,<TOPSPIN 2.1>,
      <Start of raw data processing
       efp LB = 0.3 FT_mod = 6 PKNL = 1 PHC0 = 194.811 PHC1 = 0 TDeff = 16K SI = 32K 
       data hash MD5: 32K
       70 09 6B DE 8D 9E B4 1E 1A 4C E0 AD BF 23 95 3B>)
(   5,<2015-01-30 16:02:07.933 +0000>,<NMR User>,<CHEM-NMR04>,<proc1d>,<TOPSPIN 2.1>,
      <pk fgphup PHC0 = 47.52736 PHC1 = 1.1 
       data hash MD5: 32K
       7A 6F 7F 23 71 91 DA 64 A3 74 59 CF 55 50 0A FF>)
##END=

$$ hash MD5
$$ C3 39 3B 72 8A F7 BE 04 FB 34 9A 2F 89 CC BA BF
