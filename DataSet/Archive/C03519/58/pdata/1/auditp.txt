##TITLE= Audit trail, TOPSPIN		Version 2.1
##JCAMPDX= 5.01
##ORIGIN= Bruker BioSpin GmbH
##OWNER= NMR User
$$ C:\Bruker\TOPSPIN/data/Rose/nmr/C03519/58/pdata/1/auditp.txt
##AUDIT TRAIL=  $$ (NUMBER, WHEN, WHO, WHERE, PROCESS, VERSION, WHAT)
(   1,<2014-12-16 20:48:35.703 +0000>,<Administrator>,<GH001606>,<go>,<TOPSPIN 2.1>,
      <created by zg
	started at 2014-12-16 20:47:20.109 +0000,
	POWCHK disabled, PULCHK disabled,
       configuration hash MD5:
       4F DB FE 01 64 29 F4 D5 05 17 18 DB FF 7C 08 05
       data hash MD5: 32K
       6B 12 0A 2E 42 1B 84 52 3B 24 28 DE CF F7 BD C0>)
(   2,<2014-12-16 20:48:49.578 +0000>,<Administrator>,<GH001606>,<audit>,<TOPSPIN 2.1>,
      <user comment:
       ICON-NMR User ID: MJD
       data hash MD5: 32K
       6B 12 0A 2E 42 1B 84 52 3B 24 28 DE CF F7 BD C0>)
(   3,<2014-12-16 20:48:50.640 +0000>,<Administrator>,<GH001606>,<dirdata>,<TOPSPIN 2.1>,
      <wrpa from 1 58 "C03519" "MJD" "C:\NMRdata"
       data hash MD5: 32K
       6B 12 0A 2E 42 1B 84 52 3B 24 28 DE CF F7 BD C0>)
(   4,<2015-01-30 14:22:45.796 +0000>,<NMR User>,<CHEM-NMR04>,<proc1d>,<TOPSPIN 2.1>,
      <Start of raw data processing
       efp LB = 0.3 FT_mod = 6 PKNL = 1 PHC0 = 200.3706 PHC1 = 0 TDeff = 16K SI = 32K 
       data hash MD5: 32K
       C8 35 1F FD D4 1C A3 53 5A D7 16 A8 34 C4 9E C5>)
(   5,<2015-01-30 16:20:09.842 +0000>,<NMR User>,<CHEM-NMR04>,<proc1d>,<TOPSPIN 2.1>,
      <pk fgphup PHC0 = 42.6711 PHC1 = -1.35 
       data hash MD5: 32K
       FB 70 74 97 D5 5B 60 6B 28 22 43 FF 7C 43 EE D7>)
##END=

$$ hash MD5
$$ 16 6D 5F 22 2D 36 A7 2D 78 AA 41 76 4C 7A 84 EE
