##TITLE= Audit trail, TOPSPIN		Version 2.1
##JCAMPDX= 5.01
##ORIGIN= Bruker BioSpin GmbH
##OWNER= NMR User
$$ C:\Bruker\TOPSPIN/data/Rose/nmr/C03519/51/pdata/1/auditp.txt
##AUDIT TRAIL=  $$ (NUMBER, WHEN, WHO, WHERE, PROCESS, VERSION, WHAT)
(   1,<2014-12-16 20:19:09.109 +0000>,<Administrator>,<GH001606>,<go>,<TOPSPIN 2.1>,
      <created by zg
	started at 2014-12-16 20:17:53.546 +0000,
	POWCHK disabled, PULCHK disabled,
       configuration hash MD5:
       4F DB FE 01 64 29 F4 D5 05 17 18 DB FF 7C 08 05
       data hash MD5: 32K
       66 32 85 1A 27 7A 1F B8 AF 6D 9D 46 11 B8 5B 70>)
(   2,<2014-12-16 20:19:22.218 +0000>,<Administrator>,<GH001606>,<audit>,<TOPSPIN 2.1>,
      <user comment:
       ICON-NMR User ID: MJD
       data hash MD5: 32K
       66 32 85 1A 27 7A 1F B8 AF 6D 9D 46 11 B8 5B 70>)
(   3,<2014-12-16 20:19:23.265 +0000>,<Administrator>,<GH001606>,<dirdata>,<TOPSPIN 2.1>,
      <wrpa from 1 51 "C03519" "MJD" "C:\NMRdata"
       data hash MD5: 32K
       66 32 85 1A 27 7A 1F B8 AF 6D 9D 46 11 B8 5B 70>)
(   4,<2015-01-30 14:22:41.382 +0000>,<NMR User>,<CHEM-NMR04>,<proc1d>,<TOPSPIN 2.1>,
      <Start of raw data processing
       efp LB = 0.3 FT_mod = 6 PKNL = 1 PHC0 = 198.5898 PHC1 = 0 TDeff = 16K SI = 32K 
       data hash MD5: 32K
       9B D4 D9 6C 36 53 9A 82 B1 9C 1E 81 D4 2F FB 33>)
(   5,<2015-01-30 16:17:31.205 +0000>,<NMR User>,<CHEM-NMR04>,<proc1d>,<TOPSPIN 2.1>,
      <pk fgphup PHC0 = 43.7875 PHC1 = 0 
       data hash MD5: 32K
       41 97 DD 76 A7 36 C5 C7 1A 73 36 59 0E 30 5C D3>)
##END=

$$ hash MD5
$$ CF 65 BE 9A 6D B5 66 28 28 A0 E2 B3 A4 D2 57 32
