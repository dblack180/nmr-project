##TITLE= Audit trail, TOPSPIN		Version 2.1
##JCAMPDX= 5.01
##ORIGIN= Bruker BioSpin GmbH
##OWNER= nmr user
$$ C:\Bruker\TOPSPIN/data/MJD/nmr/C02404/7/pdata/1/auditp.txt
##AUDIT TRAIL=  $$ (NUMBER, WHEN, WHO, WHERE, PROCESS, VERSION, WHAT)
(   1,<2013-06-13 13:43:25.750 +0100>,<Administrator>,<GH001606>,<go>,<TOPSPIN 2.1>,
      <created by zg
	started at 2013-06-13 13:37:02.203 +0100,
	POWCHK disabled, PULCHK disabled,
       configuration hash MD5:
       3E 28 56 27 A3 E6 96 EB AB A8 47 E1 FE 66 6F 96
       data hash MD5: 32K
       A8 3B 08 E5 73 32 9E 77 E4 F0 BF 38 E1 DB A6 7F>)
(   2,<2013-06-13 13:43:35.265 +0100>,<Administrator>,<GH001606>,<audit>,<TOPSPIN 2.1>,
      <user comment:
       ICON-NMR User ID: MJD
       data hash MD5: 32K
       A8 3B 08 E5 73 32 9E 77 E4 F0 BF 38 E1 DB A6 7F>)
(   3,<2013-06-13 13:43:35.421 +0100>,<Administrator>,<GH001606>,<dirdata>,<TOPSPIN 2.1>,
      <wrpa from 1 7 "C02404" "MJD" "C:\NMRdata"
       data hash MD5: 32K
       A8 3B 08 E5 73 32 9E 77 E4 F0 BF 38 E1 DB A6 7F>)
(   4,<2013-06-17 09:55:08.534 +0100>,<nmr user>,<NMR-1>,<proc1d>,<TOPSPIN 2.1>,
      <Start of raw data processing
       efp LB = 0.3 FT_mod = 6 PKNL = 1 PHC0 = 173.452 PHC1 = 10.892 TDeff = 16K SI = 32K 
       data hash MD5: 32K
       50 DC 74 67 48 5F 3A F6 F5 DB 01 EB 30 7C CD 99>)
##END=

$$ hash MD5
$$ E5 BD 7B 9A 96 32 45 92 BA 2F 99 C5 7B E0 D3 AC
