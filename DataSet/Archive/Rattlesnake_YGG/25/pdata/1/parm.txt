NAME             C02404                
EXPNO                25                
PROCNO                1                
Date_          20130613                
Time              16.09                
INSTRUM           spect                
PROBHD   5 mm TBI 1H/13                
PULPROG       @zgesgp_k                
TD                32768                
SOLVENT         H2O+D2O                
NS                   16                
DS                    2                
SWH            7194.245 Hz             
FIDRES         0.219551 Hz             
AQ            2.2774260 sec            
RG                  128                
DW               69.500 usec           
DE                 6.80 usec           
TE                310.0 K              
D1           2.00000000 sec            
D12          0.00002000 sec            
D16          0.00020000 sec            
D20        300.00000000 sec            
TD0                   1                

======== CHANNEL f1 ========
NUC1                 1H                
P1                11.60 usec           
P2                23.20 usec           
P12             2400.00 usec           
PL0              120.00 dB             
PL1                0.90 dB             
PL0W         0.00000000 W              
PL1W        11.63790607 W              
SFO1        600.1328266 MHz            
SP1               36.60 dB             
SPNAM1       Sinc1.1000                
SPOAL1            0.500                
SPOFFS1            0.00 Hz             

====== GRADIENT CHANNEL =====
GPNAM1         sine.100                
GPNAM2         sine.100                
GPZ1              31.00 %              
GPZ2              11.00 %              
P16             1000.00 usec           
SI                32768                
SF          600.1300072 MHz            
WDW                  EM                
SSB                   0                
LB                 0.30 Hz             
GB                    0                
PC                 1.00                
