##TITLE= Audit trail, TOPSPIN		Version 2.1
##JCAMPDX= 5.01
##ORIGIN= Bruker BioSpin GmbH
##OWNER= nmr user
$$ C:\Bruker\TOPSPIN/data/MJD/nmr/C02404/60/pdata/1/auditp.txt
##AUDIT TRAIL=  $$ (NUMBER, WHEN, WHO, WHERE, PROCESS, VERSION, WHAT)
(   1,<2013-06-16 02:05:17.062 +0100>,<Administrator>,<GH001606>,<go>,<TOPSPIN 2.1>,
      <created by zg
	started at 2013-06-15 21:03:53.343 +0100,
	POWCHK disabled, PULCHK disabled,
       configuration hash MD5:
       3E 28 56 27 A3 E6 96 EB AB A8 47 E1 FE 66 6F 96
       data hash MD5: 32K
       13 30 7F 46 0D E5 08 FD C2 74 57 94 25 FA D5 1C>)
(   2,<2013-06-16 02:05:26.484 +0100>,<Administrator>,<GH001606>,<audit>,<TOPSPIN 2.1>,
      <user comment:
       ICON-NMR User ID: MJD
       data hash MD5: 32K
       13 30 7F 46 0D E5 08 FD C2 74 57 94 25 FA D5 1C>)
(   3,<2013-06-16 02:05:26.640 +0100>,<Administrator>,<GH001606>,<dirdata>,<TOPSPIN 2.1>,
      <wrpa from 1 60 "C02404" "MJD" "C:\NMRdata"
       data hash MD5: 32K
       13 30 7F 46 0D E5 08 FD C2 74 57 94 25 FA D5 1C>)
(   4,<2013-06-17 09:56:14.553 +0100>,<nmr user>,<NMR-1>,<proc1d>,<TOPSPIN 2.1>,
      <Start of raw data processing
       efp LB = 0.3 FT_mod = 6 PKNL = 1 PHC0 = 173.452 PHC1 = 10.892 TDeff = 16K SI = 32K 
       data hash MD5: 32K
       F1 58 01 C7 70 29 79 9A D7 DE 57 7C BF 5A 24 B8>)
##END=

$$ hash MD5
$$ 9F BD 31 8D E8 A3 DF AA 94 21 71 F4 93 31 09 EC
