##TITLE= Audit trail, TOPSPIN		Version 2.1
##JCAMPDX= 5.01
##ORIGIN= Bruker BioSpin GmbH
##OWNER= nmr user
$$ C:\Bruker\TOPSPIN/data/MJD/nmr/C02404/17/pdata/1/auditp.txt
##AUDIT TRAIL=  $$ (NUMBER, WHEN, WHO, WHERE, PROCESS, VERSION, WHAT)
(   1,<2013-06-13 15:04:48.828 +0100>,<Administrator>,<GH001606>,<go>,<TOPSPIN 2.1>,
      <created by zg
	started at 2013-06-13 14:58:25.218 +0100,
	POWCHK disabled, PULCHK disabled,
       configuration hash MD5:
       3E 28 56 27 A3 E6 96 EB AB A8 47 E1 FE 66 6F 96
       data hash MD5: 32K
       32 0F 2F 81 60 E7 8A 49 A0 F2 E3 8A B6 57 BC C2>)
(   2,<2013-06-13 15:04:58.109 +0100>,<Administrator>,<GH001606>,<audit>,<TOPSPIN 2.1>,
      <user comment:
       ICON-NMR User ID: MJD
       data hash MD5: 32K
       32 0F 2F 81 60 E7 8A 49 A0 F2 E3 8A B6 57 BC C2>)
(   3,<2013-06-13 15:04:58.265 +0100>,<Administrator>,<GH001606>,<dirdata>,<TOPSPIN 2.1>,
      <wrpa from 1 17 "C02404" "MJD" "C:\NMRdata"
       data hash MD5: 32K
       32 0F 2F 81 60 E7 8A 49 A0 F2 E3 8A B6 57 BC C2>)
(   4,<2013-06-17 09:55:21.341 +0100>,<nmr user>,<NMR-1>,<proc1d>,<TOPSPIN 2.1>,
      <Start of raw data processing
       efp LB = 0.3 FT_mod = 6 PKNL = 1 PHC0 = 173.452 PHC1 = 10.892 TDeff = 16K SI = 32K 
       data hash MD5: 32K
       BF E7 3D DC 1B 43 E8 A9 07 E0 04 2A A5 C1 3B 10>)
##END=

$$ hash MD5
$$ 15 A9 16 7B 14 39 DA 0B A6 99 95 48 01 16 78 42
