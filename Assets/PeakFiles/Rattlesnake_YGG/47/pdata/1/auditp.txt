##TITLE= Audit trail, TOPSPIN		Version 2.1
##JCAMPDX= 5.01
##ORIGIN= Bruker BioSpin GmbH
##OWNER= nmr user
$$ C:\Bruker\TOPSPIN/data/MJD/nmr/C02404/47/pdata/1/auditp.txt
##AUDIT TRAIL=  $$ (NUMBER, WHEN, WHO, WHERE, PROCESS, VERSION, WHAT)
(   1,<2013-06-14 08:21:13.281 +0100>,<Administrator>,<GH001606>,<go>,<TOPSPIN 2.1>,
      <created by zg
	started at 2013-06-14 07:19:49.718 +0100,
	POWCHK disabled, PULCHK disabled,
       configuration hash MD5:
       3E 28 56 27 A3 E6 96 EB AB A8 47 E1 FE 66 6F 96
       data hash MD5: 32K
       AA A6 C0 7F E0 9A 32 42 D3 1A D3 EB 29 59 65 B3>)
(   2,<2013-06-14 08:21:22.671 +0100>,<Administrator>,<GH001606>,<audit>,<TOPSPIN 2.1>,
      <user comment:
       ICON-NMR User ID: MJD
       data hash MD5: 32K
       AA A6 C0 7F E0 9A 32 42 D3 1A D3 EB 29 59 65 B3>)
(   3,<2013-06-14 08:21:22.812 +0100>,<Administrator>,<GH001606>,<dirdata>,<TOPSPIN 2.1>,
      <wrpa from 1 47 "C02404" "MJD" "C:\NMRdata"
       data hash MD5: 32K
       AA A6 C0 7F E0 9A 32 42 D3 1A D3 EB 29 59 65 B3>)
(   4,<2013-06-17 09:56:00.419 +0100>,<nmr user>,<NMR-1>,<proc1d>,<TOPSPIN 2.1>,
      <Start of raw data processing
       efp LB = 0.3 FT_mod = 6 PKNL = 1 PHC0 = 173.452 PHC1 = 10.892 TDeff = 16K SI = 32K 
       data hash MD5: 32K
       92 5A 47 B5 AE 9E 95 3C 02 78 60 40 37 44 81 F0>)
##END=

$$ hash MD5
$$ A7 45 98 5E F0 AE 1C 38 D2 E2 A2 8A B3 B5 1C E4
