##TITLE= Audit trail, TOPSPIN		Version 2.1
##JCAMPDX= 5.01
##ORIGIN= Bruker BioSpin GmbH
##OWNER= nmr user
$$ C:\Bruker\TOPSPIN/data/MJD/nmr/C02404/65/pdata/1/auditp.txt
##AUDIT TRAIL=  $$ (NUMBER, WHEN, WHO, WHERE, PROCESS, VERSION, WHAT)
(   1,<2013-06-17 03:21:24.140 +0100>,<Administrator>,<GH001606>,<go>,<TOPSPIN 2.1>,
      <created by zg
	started at 2013-06-16 22:20:00.484 +0100,
	POWCHK disabled, PULCHK disabled,
       configuration hash MD5:
       3E 28 56 27 A3 E6 96 EB AB A8 47 E1 FE 66 6F 96
       data hash MD5: 32K
       46 81 20 5C CB 43 31 A4 8D EA F0 76 96 5E D9 69>)
(   2,<2013-06-17 03:21:33.531 +0100>,<Administrator>,<GH001606>,<audit>,<TOPSPIN 2.1>,
      <user comment:
       ICON-NMR User ID: MJD
       data hash MD5: 32K
       46 81 20 5C CB 43 31 A4 8D EA F0 76 96 5E D9 69>)
(   3,<2013-06-17 03:21:33.718 +0100>,<Administrator>,<GH001606>,<dirdata>,<TOPSPIN 2.1>,
      <wrpa from 1 65 "C02404" "MJD" "C:\NMRdata"
       data hash MD5: 32K
       46 81 20 5C CB 43 31 A4 8D EA F0 76 96 5E D9 69>)
(   4,<2013-06-17 09:56:19.560 +0100>,<nmr user>,<NMR-1>,<proc1d>,<TOPSPIN 2.1>,
      <Start of raw data processing
       efp LB = 0.3 FT_mod = 6 PKNL = 1 PHC0 = 173.452 PHC1 = 10.892 TDeff = 16K SI = 32K 
       data hash MD5: 32K
       91 8F B5 2C 54 66 B5 43 54 70 A1 EB AD 0A F1 DB>)
##END=

$$ hash MD5
$$ D9 84 9E 48 04 D0 B9 0F D6 21 8E C5 95 9A 9E 9E
