##TITLE= Audit trail, TOPSPIN		Version 2.1
##JCAMPDX= 5.01
##ORIGIN= Bruker BioSpin GmbH
##OWNER= nmr user
$$ C:\Bruker\TOPSPIN/data/MJD/nmr/C02404/5/pdata/1/auditp.txt
##AUDIT TRAIL=  $$ (NUMBER, WHEN, WHO, WHERE, PROCESS, VERSION, WHAT)
(   1,<2013-06-13 13:27:06.625 +0100>,<Administrator>,<GH001606>,<go>,<TOPSPIN 2.1>,
      <created by zg
	started at 2013-06-13 13:20:43.062 +0100,
	POWCHK disabled, PULCHK disabled,
       configuration hash MD5:
       3E 28 56 27 A3 E6 96 EB AB A8 47 E1 FE 66 6F 96
       data hash MD5: 32K
       F1 BF F6 E8 D2 6D 7D 43 D5 8A 40 9E D6 3A 1E 65>)
(   2,<2013-06-13 13:27:16.671 +0100>,<Administrator>,<GH001606>,<audit>,<TOPSPIN 2.1>,
      <user comment:
       ICON-NMR User ID: MJD
       data hash MD5: 32K
       F1 BF F6 E8 D2 6D 7D 43 D5 8A 40 9E D6 3A 1E 65>)
(   3,<2013-06-13 13:27:16.812 +0100>,<Administrator>,<GH001606>,<dirdata>,<TOPSPIN 2.1>,
      <wrpa from 1 5 "C02404" "MJD" "C:\NMRdata"
       data hash MD5: 32K
       F1 BF F6 E8 D2 6D 7D 43 D5 8A 40 9E D6 3A 1E 65>)
(   4,<2013-06-17 09:55:06.069 +0100>,<nmr user>,<NMR-1>,<proc1d>,<TOPSPIN 2.1>,
      <Start of raw data processing
       efp LB = 0.3 FT_mod = 6 PKNL = 1 PHC0 = 173.452 PHC1 = 10.892 TDeff = 16K SI = 32K 
       data hash MD5: 32K
       F6 15 5A 69 FA F4 9F 4C EA 19 87 D3 4D E2 5A 1B>)
##END=

$$ hash MD5
$$ AE 8E 09 27 1B 26 22 CC 8A 07 72 3D 0D 13 79 EF
