##TITLE= Audit trail, TOPSPIN		Version 2.1
##JCAMPDX= 5.01
##ORIGIN= Bruker BioSpin GmbH
##OWNER= nmr user
$$ C:\Bruker\TOPSPIN/data/MJD/nmr/C02404/16/pdata/1/auditp.txt
##AUDIT TRAIL=  $$ (NUMBER, WHEN, WHO, WHERE, PROCESS, VERSION, WHAT)
(   1,<2013-06-13 14:56:28.406 +0100>,<Administrator>,<GH001606>,<go>,<TOPSPIN 2.1>,
      <created by zg
	started at 2013-06-13 14:50:04.843 +0100,
	POWCHK disabled, PULCHK disabled,
       configuration hash MD5:
       3E 28 56 27 A3 E6 96 EB AB A8 47 E1 FE 66 6F 96
       data hash MD5: 32K
       70 7B 43 37 96 22 68 2B 06 17 93 51 E3 CE 3C 69>)
(   2,<2013-06-13 14:56:37.593 +0100>,<Administrator>,<GH001606>,<audit>,<TOPSPIN 2.1>,
      <user comment:
       ICON-NMR User ID: MJD
       data hash MD5: 32K
       70 7B 43 37 96 22 68 2B 06 17 93 51 E3 CE 3C 69>)
(   3,<2013-06-13 14:56:37.750 +0100>,<Administrator>,<GH001606>,<dirdata>,<TOPSPIN 2.1>,
      <wrpa from 1 16 "C02404" "MJD" "C:\NMRdata"
       data hash MD5: 32K
       70 7B 43 37 96 22 68 2B 06 17 93 51 E3 CE 3C 69>)
(   4,<2013-06-17 09:55:20.062 +0100>,<nmr user>,<NMR-1>,<proc1d>,<TOPSPIN 2.1>,
      <Start of raw data processing
       efp LB = 0.3 FT_mod = 6 PKNL = 1 PHC0 = 173.452 PHC1 = 10.892 TDeff = 16K SI = 32K 
       data hash MD5: 32K
       02 6E 07 FD B3 D8 78 1C 53 43 9C 6B 81 2D AE 40>)
##END=

$$ hash MD5
$$ 8C 61 E4 36 D3 7C 70 CD F0 B6 A8 66 C3 23 2E 45
